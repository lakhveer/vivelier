package com.lakhi.realestate.realestate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.simple.JSONObject;

import com.lakhi.realestate.dbconfig.Mail;
import com.lakhi.realestate.dbconfig.testPAYU;
import com.lakhi.realestate.vender.dao.listingDao;
import com.lakhi.realestate.vender.model.listCategory;
import com.lakhi.realestate.vender.model.vendorListing;
import com.lakhi.realestate.vender.service.ListingService;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;


@Path("/listing")
public class ListingResource {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;
	
	@Context
	HttpSession session;
	
	ListingService ls = new listingDao();
	
	@GET
	@Path("/country")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getCountries(){	
		JSONObject json = new JSONObject();
		json.put("countries",ls.getAllCountries());
		String z = json.toString();
		return z;
		
	}
	
	@GET
	@Path("/mainCategory")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<listCategory> getMainCategories(){
		return ls.getMainCategories();  
	}
	
	@GET
	@Path("/subCategory/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<listCategory> getSubCategories(@PathParam("id") int id,@PathParam("name") String name){
		return ls.getSubCategories(name.replaceAll("-", " "));  
	}
	
	@POST
	@Path("/createListing")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String createNewListing(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("mcategory") String mCategory, @FormDataParam("scategory") int sCategory,
			@FormDataParam("listPlan") int listPlan, @FormDataParam("title") String title, @FormDataParam("cn") String companyName,
			@FormDataParam("cp") String contactPerson, @FormDataParam("address") String address, @FormDataParam("city") String city,
			@FormDataParam("state") String state, @FormDataParam("country") String country, @FormDataParam("phone") String phone,
			@FormDataParam("fax") String fax, @FormDataParam("email") String email,@FormDataParam("website") String website,
			@FormDataParam("description") String description) throws FileNotFoundException, IOException{
		
		//System.out.println(fileDetail.getFileName());
		
		/*if(listPlan == 1){
			System.out.println("FREE");
		} else if(listPlan == 30){
			System.out.println("Basic");
		} else if(listPlan == 70){
			System.out.println("Pro");
		} else if(listPlan == 1000){
			System.out.println("Preimum ");
		}*/
		
		String fileName = new Random().nextInt(1000)+"-"+fileDetail.getFileName();
		String fileLocation = request.getRealPath("/images/vendor/listing") + "/" + fileName;

		System.out.println(fileLocation);
		try {
			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		int maincategory = ls.getMainCategorieByName(mCategory.replaceAll("-", " "));
		vendorListing v = new vendorListing();
		v.setMainCategory(maincategory);
		v.setSubCategory(sCategory);
		v.setMainImage(fileName);
		v.setSignupPlan(String.valueOf(listPlan));
		v.setTitle(title);
		v.setContactPerson(contactPerson);
		v.setCompanyName(companyName);
		String add = address+","+city+","+state+","+country;
//		v.setAddress(add);
		v.setAddress(address);
		v.setCity(city);
		v.setState(state);
		v.setCountry(country);
		v.setPhone(phone);
		v.setFax(fax);
		v.setEmail(email);
		v.setWebsite(website);
		v.setDescription(description);
		
		Map<String, String> m = (Map<String, String>) testPAYU.createNewListing(v);
		JSONObject json = new JSONObject();
		json.putAll(m);
		json.put("plan", v.getSignupPlan());
		json.put("title", v.getTitle());
		json.put("contactPerson", v.getContactPerson());
		json.put("companyName", v.getCompanyName());
		json.put("address", v.getAddress());
		json.put("city", v.getCity());
		json.put("state", v.getState());
		json.put("country", v.getCountry());
		json.put("phone", v.getPhone());
		json.put("email", v.getEmail());
		json.put("description", v.getDescription());
		
		String z = json.toString().replace("\\/", "/");
		
		session = request.getSession();
		session.setAttribute("list", v);
		session.setMaxInactiveInterval(5*60);
		
		return z;
	}
	
	@POST
	@Path("/listCreate")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String createList(vendorListing v) throws MandrillApiError, IOException{
		String plan = "";
		if(v.getSignupPlan().equals("1")){
			plan = "Free List";
		} else if(v.getSignupPlan().equals("30")){
			plan = "Standard 1 Yr";
		} else if(v.getSignupPlan().equals("70")){
			plan = "Standard 3 Yr";
		} else if(v.getSignupPlan().equals("1000")){
			plan = "Featured 1 Yr";
		} 
		v.setSignupPlan(plan);
	
		ls.addListingInVendor(v);
		ls.createListing(v);
		
		Mail.createList(v.getEmail(), v.getContactPerson(), v.getSignupPlan()); // mail
		
		return "{\"val\":\"Done\"}";
	}
	
	@POST
	@Path("/createNewList")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String newList(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("vId") int vid, @FormDataParam("mCategory") String mCategory,
			@FormDataParam("sCategory") int sCategory, @FormDataParam("title") String title, @FormDataParam("url") String url,
			@FormDataParam("description") String description, @FormDataParam("address") String address, 
			@FormDataParam("city") String city, @FormDataParam("state") String state, @FormDataParam("country") String country){
		
		
		String fileName = new Random().nextInt(1000)+"-"+fileDetail.getFileName();
		String fileLocation = request.getRealPath("/images/vendor/listing") + "/" + fileName;
		//System.out.println(fileLocation);
		try {
			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String mail = (String) request.getSession().getAttribute("vmail");
		int maincategory = ls.getMainCategorieByName(mCategory.replaceAll("-", " "));
		vendorListing v = new vendorListing();
		v.setEmail(mail);
		v.setMainCategory(maincategory);
		v.setSubCategory(sCategory);
		v.setTitle(title);
		v.setWebsite(url);
		v.setDescription(description);
		v.setMainImage(fileName);
		v.setCompanyName("");
		v.setFax("");
		v.setAddress(address);
		v.setCity(city);
		v.setState(state);
		v.setCountry(country);
		
		ls.createNewList(v);
		
		return "{\"val\":\"Done\"}";
	}
	
	@GET
	@Path("/allListing/{email}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<vendorListing> getAllListing(@PathParam("email") String email){
		return ls.getAllListing(email);
	}
	
	@GET
	@Path("/getList/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<vendorListing> getList(@PathParam("id") int id){
		return ls.getListById(id);
	}
	
	@PUT
	@Path("/updateList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String updateList(vendorListing v){
		int maincategory = ls.getMainCategorieByName(v.getCategoryName().replaceAll("-", " "));
		v.setMainCategory(maincategory);
		ls.ListUpdate(v);
		return "{\"val\":\"Done\"}";
	}
	
	@POST
	@Path("/updateCoverImg")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String updateCoverImage(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail,@FormDataParam("id") int id){
		
		String fileName = new Random().nextInt(1000)+"-"+fileDetail.getFileName();
		String fileLocation = request.getRealPath("/images/vendor/listing") + "/" + fileName;
		try {
			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		ls.updateCoverImg(id, fileName);
		
		return "{\"val\":\"Done\"}";
	}
	
	@GET
	@Path("/getAllList")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<vendorListing> getAllList(){
			return ls.getAllList();
	}
	
	@GET
	@Path("/categories")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<listCategory> getCategory(){
		return ls.getCategories(); 
	}
	
	@GET
	@Path("/singleListData/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> singleList(@PathParam("id") String id){
		return ls.getSingleList(id);
	}
	
	@GET
	@Path("/ListingData/{mId}/{sId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingData(@PathParam("mId") String mId, @PathParam("sId") String sId){
		return ls.ListingData(mId,sId);
	}
	
	@GET
	@Path("/ListingDataByState/{mId}/{sId}/{st}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingDataByState(@PathParam("mId") String mId, @PathParam("sId") String sId, @PathParam("st") String st){
		return ls.ListingByState(mId, sId, st);
	}
	
	@GET
	@Path("/ListingDataByCountry/{mId}/{sId}/{st}/{co}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingDataByCountry(@PathParam("mId") String mId, @PathParam("sId") String sId, @PathParam("st") String st, @PathParam("co") String co){
		//System.out.println("State = " + st + "\nCountry = " + co);
		return ls.ListingByCountry(mId, sId, st, co);
	}
	
	@GET
	@Path("/mainCategoryListing/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getMainCateListData(@PathParam("name") String name){
		return ls.mainListingData(name);
	}
	
	@GET
	@Path("/getListingDataByAddress/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> listingDataByAddress(@PathParam("name") String name){
		return ls.getListingDataByAddress(name);
	}
	
	@DELETE
	@Path("/removeList/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String removeList(@PathParam("id") int id){
		ls.deleteListing(id);
		return "{\"val\":\"Done\"}";
	}
	
	@GET
	@Path("/getListingAddress")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingAddress(){
		return ls.getListAddress();
	}
	
	@GET
	@Path("/getListingCity")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingCity(){
		return ls.getListCity();
	}
	
	@GET
	@Path("/getListingState")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingState(){
		return ls.getListState();
	}
	
	@GET
	@Path("/getListingCountry")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getListingCountry(){
		return ls.getListCountry();
	}
	
	@GET
	@Path("/getKeywords/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getAllKeywords(@PathParam("key") String key){
		return ls.keywordSearch(key);
	}
	
	@GET
	@Path("/getVendorListKeywords/{key}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorListing> getVendorKeywords(@PathParam("key") String key){
		String email = (String) request.getSession().getAttribute("vmail");
		return ls.vListkeywordSearch(key, email);
	}
}
