package com.lakhi.realestate.realestate.adminResource;

import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.lakhi.realestate.admin.dao.loginDao;
import com.lakhi.realestate.admin.model.login;
import com.lakhi.realestate.admin.service.loginService;
import com.lakhi.realestate.dbconfig.secure;

@Path("/admin")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginResource {

	@Context
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;
	
	@Context
	HttpSession session;
	
	loginService login = new loginDao();

	public loginService getLogin() {
		return login;
	}

	@POST
	@Path("/login")
	public String login(login l) {
		boolean z = login.isLogin(l);
		if (z) {
			String token = UUID.randomUUID().toString();
			String tok = l.getEmail() + "--" + l.getPassword();
			String encoded = secure.encodedToken(tok);
			Cookie ck = new Cookie("aLogin", encoded);
			Cookie ck1 = new Cookie("aToken", token);
//			ck.setMaxAge(20 * 60);
//			ck1.setMaxAge(20 * 60);
			response.addCookie(ck);
			response.addCookie(ck1);

			session = request.getSession();
			session.setAttribute("login", token);
			session.setAttribute("email", l.getEmail());
			session.setMaxInactiveInterval(10 * 60);

			return "{\"val\":\"Success\"}";
		} else {
			return "{\"val\":\"Invalid email or password\"}";
		}
	}
	
	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String vLogout() {
		String z = (String) request.getSession().getAttribute("email");
		HttpSession s = request.getSession(false);
		s.invalidate();

		Cookie cookie = null;
		Cookie[] ck = request.getCookies();
		for (int i = 0; i < ck.length; i++) {
			cookie = ck[i];
			cookie.setMaxAge(0);
		}
		return "{\"val\":\"logout\"}";
	}

}
