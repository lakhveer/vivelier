package com.lakhi.realestate.realestate.adminResource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.lakhi.realestate.admin.dao.adminDao;
import com.lakhi.realestate.admin.model.login;
import com.lakhi.realestate.admin.service.adminService;
import com.lakhi.realestate.dbconfig.DB;
import com.lakhi.realestate.dbconfig.Mail;
import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.vender.model.vendor;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("admin")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AdminResoursce {

	@Context
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;
	
	@Context
	HttpSession session;
	
	adminService service = new adminDao();

	public adminService getService() {
		return service;
	}
	
	@GET
	@Path("/{email}")
	public List<login> getAdminProfile(@PathParam("email") String email){
		return service.getProfile(email);
	}
	
	@PUT
	@Path("/{id}")
	public String updateProfile(@PathParam("id") int id, login l) throws MandrillApiError, IOException{
		service.updateProfile(l);
		if(l.getPassword() != null){
			Mail.send(l.getEmail(),l.getFirstName(),l.getPassword()); // send mail when password update
		}
		return "{\"val\":\"Success\"}";
	}
	
	@GET
	@Path("/vendors")
	public List<vendor> getAllVendors(){
		return service.getAllVendors();
	}
	
	@GET
	@Path("/vendorsByPlan/{plan}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendor> allVendorsByPlan(@PathParam("plan") String plan){
		return service.getVendorsByPlan(plan);
	}
	
	@GET
	@Path("/vendors/{id}")
	public List<vendor> getVendorById(@PathParam("id") int id){
		return service.getVendorById(id);
	}
	
	@PUT
	@Path("/vendor/{id}")
	public String resetVendorPassword(@PathParam("id") int id, vendor v) throws MandrillApiError, IOException{
		String pwd = new BigInteger(40,new Random()).toString(32);
		v.setPassword(pwd);
		
		service.resetVendorPassword(v);
		Mail.send(v.getEmail(),v.getFirst_name(),v.getPassword()); // send mail when password update
		
		return "{\"val\":\"Success\"}";
	}
	
	@PUT
	@Path("/vendor/deactivate/{id}")
	public String accountDeactivate(@PathParam("id") int id){
			service.deactiveVendorAccount(id);
		return "{\"val\":\"Success\"}";
	}
	
	@PUT
	@Path("/vendor/activate/{id}")
	public String accountActivate(@PathParam("id") int id){
			service.activeVendorAccount(id);
		return "{\"val\":\"Success\"}";
	}
	
	@GET
	@Path("/backupDatabase")
	public String backupDatabase() throws IOException{
		String fileLocation = request.getRealPath("/DBbackup/");
		System.out.println(fileLocation);
		DB.backupDatabase(fileLocation);
//		String downloadFile = "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/DBbackup/";
		String downloadFile = dbconfig.getBaseURL()+"DBbackup/";
		String z = DB.downloadFile(downloadFile);
		return "{\"val\":\""+z+"\"}";
	}
	
	@POST
	@Path("/img")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String updateProfile(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("email") String email) {

		String img = new Random().nextInt(1000) + "_" + fileDetail.getFileName();
		
		login l = new login();
		l.setEmail(email);
		l.setImg(img);
		service.updateProfilePic(l);
		
//		System.out.println("path = " + fileDetail);
		String fileLocation = request.getRealPath("/images/admin")+"/"+ img;
		try {

			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "{\"val\":\"Success\"}";
	}
	
	@GET
	@Path("/countVendors")
	public List<vendor> totalVendors(){
		return service.countVendors();
	}
	
	@POST
	@Path("/updateAboutUs")
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updateAbout(@FormParam("about") String data){
		service.updateAboutUs(data);
		return "Done";
	}
	
	@GET
	@Path("/getAboutData")
	@Produces(MediaType.TEXT_HTML)
	public String getAbout(){
		return service.getAboutData();
	}
	
	@POST
	@Path("/updatePrivacy")
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updatePrivacy(@FormParam("privacy") String data){
		service.updatePrivacy(data);
		return "Done";
	}
	
	@GET
	@Path("/getPrivacyData")
	@Produces(MediaType.TEXT_HTML)
	public String getPrivacy(){
		return service.getPrivacyData();
	}
	
	@POST
	@Path("/updateTermsConditions")
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updateTerms(@FormParam("terms") String data){
		service.updateTermsConditions(data);
		return "Done";
	}
	
	@GET
	@Path("/getTermsConditions")
	@Produces(MediaType.TEXT_HTML)
	public String getTerms(){
		return service.getTermsConditions();
	}
	
	@POST
	@Path("/updateServices")
	@Produces(MediaType.APPLICATION_FORM_URLENCODED)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updateServices(@FormParam("services") String data){
		service.updateServices(data);
		return "Done";
	}
	
	@GET
	@Path("/getServices")
	@Produces(MediaType.TEXT_HTML)
	public String getServices(){
		return service.getServicesData();
	}
	
}
