package com.lakhi.realestate.realestate.adminResource;

import java.io.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import com.lakhi.realestate.admin.dao.hotelDao;
import com.lakhi.realestate.admin.model.hotels;
import com.lakhi.realestate.admin.service.hotelService;
import com.lakhi.realestate.dbconfig.csv;

@Path("admin")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HotelResource {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	hotelService service = new hotelDao();

	@GET
	@Path("/hotels/{data}")
	public String getCityOrCountry(@PathParam("data") String data) {
		List<String> l = null;
		if (data.toUpperCase().equals("CITY")) {
			l = service.getCity();
		} else if (data.toUpperCase().equals("COUNTRY")) {
			l = service.getCountries();
		} else if (data.toUpperCase().equals("STATE")) {
			l = service.getState(data);
		}
		String json = new Gson().toJson(l);
		return json;
	}

	@POST
	@Path("/hotels/{data}")
	public String exportHotehDataInCSV(@PathParam("data") String data) throws IOException, InterruptedException {
		String[] city = data.split("=");
		String[] aa = city[1].split("\\)-");
		
		// String path =
		// "/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/webapp/DBbackup/";
		String path = request.getRealPath("/DBbackup/");
		//System.out.println(path);
		String data1 = "";
		String cityName[] = city[1].split("\\)-");
		if (city[0].toUpperCase().equals("CITY")) {
			List<hotels> l = service.getHotelByCity(aa[1]);
			csv.createCSVFile(aa[1], l, path); // create csv file
			data1 = csv.downloadCSVFile(aa[1], l, path); // download file
		} else if (city[0].toUpperCase().equals("COUNTRY")) {
			 System.out.println("country = " + city[1]);
			 List<hotels> l = service.getHotelByCountry(city[1]);
			 csv.createCSVFile(city[1], l, path); // create csv file
				data1 = csv.downloadCSVFile(city[1], l, path); // download file
			/*String zz[] = cityName[0].split("=");
			System.out.println("COUNTRY = " + zz[0]);
			List<hotels> l = service.getHotelByCountry(zz[1]);
			csv.createCSVFile(zz[1], l, path); // create csv file
			data1 = csv.downloadCSVFile(zz[1], l, path); // download file
			System.out.println(data1);*/
		}
		return "{\"val\":\"" + data1 + "\"}";
	}

	
	@POST
	@Path("/hotels")
	public String importCSVData(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail) {

		// String path =
		// "/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/webapp/DBbackup/csvFile.csv";
		String path = request.getRealPath("/DBbackup/csvFile.csv");
		System.out.println(path);
		try {
			File file = new File(path);
			Reader reader = new InputStreamReader(is);
			BufferedReader fin = new BufferedReader(reader);
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			String s;
			while ((s = fin.readLine()) != null) {
				bw.write(s);
				bw.newLine();
			}
			bw.close();

			csv.uploadCSVFile(file);
			if (file.exists()) {
				file.delete();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "{\"val\":\" Your Data Successfully Upload. \"}";
	}

	@GET
	@Path("/countHotels")
	public List<hotels> countHotels() {
		return service.totalHotels();
	}

	@GET
	@Path("/allHotels/{limit}")
	public List<hotels> getAllHotels(@PathParam("limit") int limit) {
		// System.out.println("Limit = " + limit);
		List<hotels> l = null;

		if (limit == 1) {
			l = service.getAllHotels(limit - 1);
		} else {
			l = service.getAllHotels(limit * 10);
		}
		return l;
	}

	@GET
	@Path("/hotel/{id}")
	public List<hotels> getHotelById(@PathParam("id") int id) {
		return service.getHotelById(id);
	}

	@PUT
	@Path("/hotel/{id}")
	public String updateHotel(@PathParam("id") int id, hotels h) {
		service.updateHotel(h);
		return "{\"val\":\"Done\"}";
	}

	@POST
	@Path("/addNewHotels")
	public String addNewHotel(hotels h) {
		service.addNewHotel(h);
		return "{\"val\":\"Done\"}";
	}

	@POST
	@Path("/addGallery")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String addGallery(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("url") String url,
			@FormDataParam("id") int id) {

		hotels h = new hotels();
		h.setId(id);
		h.setUrl(url);
		h.setImages(fileDetail.getFileName());

		service.addHotelGallery(h);

		String fileLocation = request.getRealPath("/images/admin/hotels") + "/" + fileDetail.getFileName();

		try {

			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return "{\"val\":\"Done\"}";
	}

	@GET
	@Path("/getHotelsBySearch/{data}")
	public List<hotels> getHotelsBySearch(@PathParam("data") String data){
		System.out.println("Data = " +  data);
		return service.totalHotelsBySearch(data);
	}
	
	@GET
	@Path("/getHotelByCountry/{limit}/{country}")
	public List<hotels> getHotelByCountry(@PathParam("limit") int limit, @PathParam("country") String country){
//		System.out.println("Country = " + country);
		return service.allHotelByCountry(limit, country);
	}
	
	@GET
	@Path("/hotelImages/{hotelId}")
	public List<hotels> getHotelImages(@PathParam("hotelId") int hotelId){
		return service.getHotelGallery(hotelId);
	}
	
	@GET
	@Path("/GETALLHOTELS")
	public List<hotels> hotels(){
		return service.getAllHotels();
	}
	
	@DELETE
	@Path("/deleteHotel/{id}")
	public String removeHotel(@PathParam("id") int id){
		service.deleteHotel(id);
		return "{\"val\":\"Done\"}";
	}
}
