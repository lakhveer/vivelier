package com.lakhi.realestate.realestate.freeVendorResource;

import java.io.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.lakhi.realestate.dbconfig.Mail;
import com.lakhi.realestate.freeVendors.dao.AdsDao;
import com.lakhi.realestate.freeVendors.dao.freeVendorDao;
import com.lakhi.realestate.freeVendors.models.Ads;
import com.lakhi.realestate.freeVendors.models.freeVendors;
import com.lakhi.realestate.freeVendors.services.AdService;
import com.lakhi.realestate.freeVendors.services.freeVendorServices;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;

@Path("/freeVendors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class freeVendorResource {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@Context
	HttpSession session;

	freeVendorServices freevendor = new freeVendorDao();
	AdService ad = new AdsDao();

	@POST
	public String addFreeVendor(freeVendors f) throws MandrillApiError, IOException {
		freevendor.addNewFreeVendor(f); // add new vendor free signup
		Mail.signup(f.getFirstName(),f.getEmail(),f.getFirstName()); // send mail when free signup
		return "{\"val\":\"Success\"}";
	}
	
	@POST
	@Path("/createAd")
	@Produces({MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_JSON})
	@Consumes({MediaType.MULTIPART_FORM_DATA,MediaType.APPLICATION_JSON})
	public String createNewAd(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail,
			@FormDataParam("fList") String fList, @FormDataParam("title") String title,
			@FormDataParam("description") String description, @FormDataParam("vId") int vid){
		
		String img = new Random().nextInt(1000) + "_" + fileDetail.getFileName();
		String fileLocation = request.getRealPath("/images/vendor/Ads") + "/" + img;
		//System.out.println(fileLocation);
		try {
			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		List aa = new ArrayList<>();
		aa.add(fList);
		
		Ads a = new Ads();
		a.setTitle(title);
		a.setDescription(description);
		a.setFeatures(aa);
		a.setImg(fileDetail.getFileName());
		a.setVendorId(vid);
		
		ad.createAds(a); // create new Ad's  
		
		return "{\"val\":\"Success\"}";
	}

	@GET
	@Path("/getAllAds/{email}")
	public List<Ads> getAds(@PathParam("email") String email){
		return ad.getAllAds(email);
	}
	
	@GET
	@Path("/getSingleAd/{id}")
	public List<Ads> getAd(@PathParam("id") int id){
		return ad.getAdById(id);
	}
	
	@PUT
	@Path("/updateAd/{id}/{list}")
	public String editAd(@PathParam("id") int id,@PathParam("list") String list, Ads a){
		String li = list.replace("[", "");
		List aa = new ArrayList<>();
		aa.add(li);
		a.setFeatures(aa);
		ad.updateAd(a);
		return "{\"val\":\"Success\"}";
	}
	
	@DELETE
	@Path("/deleteAd/{id}")
	public String delAd(@PathParam("id") int id){
		System.out.println("ID = " + id);
		ad.deleteAd(id);
		return "{\"val\":\"Success\"}";
	}
	
}
