package com.lakhi.realestate.realestate;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.lakhi.realestate.dbconfig.Mail;
import com.ecwid.mailchimp.MailChimpException;
import com.lakhi.realestate.dbconfig.*;
import com.lakhi.realestate.dbconfig.propertyConfig;
import com.lakhi.realestate.vender.dao.vendorContactDao;
import com.lakhi.realestate.vender.model.rating;
import com.lakhi.realestate.vender.model.vendorContact;
import com.lakhi.realestate.vender.service.contactService;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

@Path("/contact")
public class contactResource {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@Context
	HttpSession session;
	
	contactService cs = new vendorContactDao();
	
	@POST
	@Path("/contactVendor")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String vendor_contact(vendorContact vc) throws MandrillApiError, IOException{
		Mail.contactVendor(vc);
		cs.contactVendor(vc);
		return "{\"val\":\"Done\"}";
	}
	
	@POST
	@Path("/addRating")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String addrating(rating r) throws UnsupportedEncodingException{
		String vmail = (String)request.getSession().getAttribute("vmail");
		r.setVmail(vmail);
		int id = cs.getListById(r.getListName());
		r.setListId(id);
		
		Cookie[] cookies = request.getCookies();
		Cookie cookie;
		  if(cookies != null) {
		      for (int i = 0; i < cookies.length; i++) {
		          cookie=cookies[i];
		          if (cookie.getName().equals("vmail")) {
		        	  r.setVmail(URLDecoder.decode(cookie.getValue(), "utf8"));
		          }
		          if (cookie.getName().equals("fmail")) {
		        	  r.setVmail(URLDecoder.decode(cookie.getValue(), "utf8"));
		          }
		          if (cookie.getName().equals("gmail")) {
		        	  r.setVmail(URLDecoder.decode(cookie.getValue(), "utf8"));
		          }
		          if (cookie.getName().equals("tmail")) {
		        	  r.setVmail(cookie.getValue());
		          }
		       }
		   }
		
		cs.addRating(r);
		return "{\"val\":\"Done\"}";
	}
	
	@GET
	@Path("/ratingByList/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<rating> getAllRatingsByList(@PathParam("id") String id){
		return cs.getRatingByList(id);
	}
	
	@GET
	@Path("/twitterOAuthURL/{lid}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String twitterURL(@PathParam("lid") String id) throws TwitterException, FileNotFoundException, IOException{
		
	    Properties prop = propertyConfig.getMailChimProperty();
	    
		 Twitter twitter = new TwitterFactory().getInstance();
	        twitter.setOAuthConsumer(prop.getProperty("CONSUMER_KEY"), prop.getProperty("CONSUMER_KEY_SECRET"));
	        RequestToken requestToken = twitter.getOAuthRequestToken();
	     
	        request.getSession().setAttribute("twitter", twitter);
	        request.getSession().setAttribute("requestToken", requestToken);
	        request.getSession().setAttribute("lid", id);
	        
	        String url = requestToken.getAuthorizationURL();
		return "{\"val\":\""+url+"\"}";
	}
	
	@GET
	@Path("/twitterData")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public void getTwitterData() throws IOException{
		Twitter twitter = (Twitter) request.getSession().getAttribute("twitter");
		RequestToken requestToken = (RequestToken) request.getSession().getAttribute("requestToken");
		String verifier = request.getParameter("oauth_verifier");
		try {
			AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
		    request.getSession().removeAttribute("requestToken");
		   
		    Cookie ck = new Cookie("tmail", accessToken.getScreenName());
			response.addCookie(ck);
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.sendRedirect(dbconfig.getBaseURL()+"reviews/" + request.getSession().getAttribute("lid"));
	}
	
	@POST
	@Path("/subscribeMail")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String subscribe(vendorContact vc) throws IOException, MailChimpException{
		String data = null;
		//String data = subscribe.subscribeMail(vc.getEmail(), vc.getName());
		int i = cs.getSubscriber(vc.getEmail());
		if(i == 0){
			cs.addSubscribe(vc.getName(), vc.getEmail());
			data = subscribe.subscribeMail(vc.getEmail(), vc.getName());
		} else if(i == 1){
			data = ""+i;
		}
		return "{\"val\":\""+data+"\"}";
	}
	
	@POST
	@Path("/contactus")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String contactUS(vendorContact vc) throws FileNotFoundException, IOException, MandrillApiError{
		cs.contactUs(vc);
		Mail.contactUsMail(vc);
		String data = "Done";
		return "{\"val\":\""+data+"\"}";
	}
	
	@GET
	@Path("/allContactUsList")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendorContact> getAllContactUsList(){
		return cs.getContactUsList();
	}
	
	@DELETE
	@Path("/removeContactUs/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String deleteContactUs(@PathParam("id") int id){
		cs.removeContactUs(id);
		return "{\"val\":\"Done\"}";
	}
	
	@GET
	@Path("/checkEmail/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String getEmail(@PathParam("email") String email){
		int z = cs.checkEmail(email);
		String data = String.valueOf(z);
		return "{\"val\":\""+data+"\"}";
	}
	
}
