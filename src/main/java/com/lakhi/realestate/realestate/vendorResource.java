package com.lakhi.realestate.realestate;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.lakhi.realestate.admin.model.hotels;
import com.lakhi.realestate.dbconfig.Mail;
import com.lakhi.realestate.dbconfig.csv;
import com.lakhi.realestate.dbconfig.secure;
import com.lakhi.realestate.dbconfig.testPAYU;
import com.lakhi.realestate.vender.dao.vendorDao;
import com.lakhi.realestate.vender.service.VendorService;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.lakhi.realestate.vender.model.vendor;

@Path("vendor")
public class vendorResource {

	@Context
	HttpServletRequest request;

	@Context
	HttpServletResponse response;

	@Context
	HttpSession session;
	
	private VendorService vendor = new vendorDao();

	public VendorService getVendor() {
		return vendor;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String addVendor(vendor v) throws FileNotFoundException, IOException {
		// vendor.addVendor(v); //add vendor

		Map<String, String> m = (Map<String, String>) testPAYU.payu(v);
		JSONObject json = new JSONObject();
		json.putAll(m);
		String z = json.toString();

		session = request.getSession();
		session.setAttribute("Semail", v.getEmail());
		session.setAttribute("Sfname", v.getFirst_name());
		session.setAttribute("Slname", v.getLast_name());
		session.setAttribute("Sphone", v.getPhone());
		session.setAttribute("Splan", v.getAmount());

		return z;
	}

	@POST
	@Path("/signUpVendor")
	public String signUpVendor(vendor v) {
		// System.out.println("email = " + v.getEmail());
		vendor.addVendor(v); // add vendor
		return "{\"val\":\"Success\"}";
	}

	@GET
	@Path("signup/{email}")
	public String signupMail(@PathParam("email") String email) throws MandrillApiError, IOException {
		vendor v = new vendor();
		String pwd = new BigInteger(40, new Random()).toString(32);
		v.setPassword(pwd);
		v.setEmail(email);

		// vendor.createPassword(v);
		System.out.println("Email = " + email + "\n" + request.getSession().getAttribute("Sfname"));

		String fname = (String) request.getSession().getAttribute("Sfname");
		System.out.println(v.getEmail() + "\n" + v.getPassword());
		Mail.signup(fname, email, pwd); // send mail when signup

		// session.invalidate();

		return "{\"val\":\"Success\"}";
	}

	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String login(vendor v) {
		String z = vendor.isLogin(v.getEmail(), v.getPassword());
		String status = vendor.checkLoginStatus(v.getEmail());
		if (status.equals("1")){
			return "{\"isLog\":\"isLog\"}";
		} else {
		if (z.equals("1")) {
			String token = UUID.randomUUID().toString();
			String tok = v.getEmail() + "--" + v.getPassword();
			String encoded = secure.encodedToken(tok);

			Cookie ck = new Cookie("login", encoded);
			Cookie ck1 = new Cookie("token", token);
			ck.setMaxAge(5 * 60);
			ck1.setMaxAge(5 * 60);
			response.addCookie(ck);
			response.addCookie(ck1);

			session = request.getSession();
			session.setAttribute("login", token);
			session.setAttribute("vmail", v.getEmail());
			
			Integer loginStatusID = 0;
			List<vendor> l = vendor.getVendorByEmail(v.getEmail());
			for (vendor vv : l) {
				vendor aa = (vendor) vv;
				session.setAttribute("vPlan", aa.getAmount());
				vendor.addLoginStatus(aa.getId(),vv.getEmail(),token);
				loginStatusID = vendor.checkLoginStatusByEmail(vv.getEmail());
			}
			session.setMaxInactiveInterval(10 * 60);
			Cookie loginStatus = new Cookie("loginStatus", String.valueOf(loginStatusID));
//			loginStatus.setPath("/vivelier/");
			loginStatus.setPath("/realestate/");
			//loginStatus.setMaxAge(0);
			response.addCookie(loginStatus);
			return "{\"val\":\"Success\"}";
		} else if (z.equals("0")) {
			return "{\"deactive\":\"deactive\"}";
		} else {
			return "{\"inval\":\"inval\"}";
		}
		}
	}
	
	@POST
	@Path("/forgotPassword")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String resetPassword(vendor v) throws MandrillApiError, IOException {
		String ret = null;
		int i = vendor.checkEmail(v.getEmail());
		if (i == 0) {
			ret = "invalid";
		} else {
			String pwd = new BigInteger(40, new Random()).toString(32);
			v.setPassword(pwd);
			vendor.forgotPassword(v);

			System.out.println(v.getPassword());
			Mail.forgotVendorPassword(v.getEmail(), v.getPassword());
			ret = "success";
		}
		return "{\"val\":\"" + ret + "\"}";
	}

	@GET
	@Path("/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<vendor> getVendorData(@PathParam("email") String email) {
		return vendor.getVendorByEmail(email);
	}

	@PUT
	@Path("/{id}")
	public String updateProfile(@PathParam("id") int id, vendor v) throws MandrillApiError, IOException {
		// System.out.println("id = " + l.getPassword());
		vendor.updateVendor(v);
		if (v.getPassword() != null) {
			Mail.send(v.getEmail(), v.getFirst_name(), v.getPassword()); // send
																			// mail
		}
		return "{\"val\":\"Success\"}";
	}

	@POST
	@Path("/img")
	@Consumes({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON })
	public String updateProfile(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition fileDetail, @FormDataParam("email") String email) {

		String img = new Random().nextInt(1000) + "_" + fileDetail.getFileName();

		vendor v = new vendor();
		v.setEmail(email);
		v.setImage(img);

		System.out.println("img = " + v.getImage() + "\nemail = " + v.getEmail());

		vendor.updateVendorProfilePic(v);

		// String fileLocation =
		// "/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/webapp/images/vendor/"+
		// img;
		String fileLocation = request.getRealPath("/images/vendor") + "/" + img;
		//System.out.println("path = " + request.getServletContext().getContextPath());
		try {

			FileOutputStream out = new FileOutputStream(new File(fileLocation));
			int read = 0;
			byte bytes[] = new byte[1024];
			out = new FileOutputStream(new File(fileLocation));
			while ((read = is.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return "{\"val\":\"Success\"}";
	}

	@GET
	@Path("exportHotelSearch/{data}")
	public String exportHotels(@PathParam("data") String data) throws IOException {
		String fileName = "hotels";
		String fileLocation = request.getRealPath("/DBbackup/");
		String link = "";
		if (data.equals("empty")) {
			List<hotels> h = vendor.getAllHotels();
			csv.createCSVFile(fileName, h, fileLocation);
			link = csv.downloadCSVFile(fileName, h, fileLocation);
		} else {
			List<hotels> h = vendor.getHotelBySearch(data);
			csv.createCSVFile(fileName, h, fileLocation);
			link = csv.downloadCSVFile(fileName, h, fileLocation);
		}
		return "{\"val\":\"" + link + "\"}";
	}

	@POST
	@Path("/updateVendorPlan")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String updateVendorPlan(vendor v) throws FileNotFoundException, IOException {
		Map<String, String> m = (Map<String, String>) testPAYU.updatePlan(v);
		JSONObject json = new JSONObject();
		json.putAll(m);
		String z = json.toString();

		HttpSession se = request.getSession();
		se.setAttribute("thanksEmail", v.getEmail());
		se.setAttribute("thanksPlan", v.getAmount());
		se.setAttribute("thanksFname", v.getFirst_name());

		return z;
	}

	@GET
	@Path("/mailForVendorPlan/{mail}/{vendorPlan}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String SendMailUpdateVendorPlan(@PathParam("mail") String mail, @PathParam("vendorPlan") String vendorPlan)
			throws MandrillApiError, IOException {
		System.out.println(mail + "\n" + vendorPlan);
		vendor.updateVendorPlan(vendorPlan, mail);
		String fn = (String) request.getSession().getAttribute("thanksFname");
		Mail.updateVendorPlanMail(mail, fn, vendorPlan);

		HttpSession se = request.getSession(false);
		se.getAttribute("thanksEmail");
		se.getAttribute("thanksPlan");
		se.getAttribute("thanksFname");
		// se.invalidate();

		return "{\"val\":\"Done\"}";
	}

	@POST
	@Path("/countHotelsBySearch")
	@Produces({ "application/json" })
	@Consumes({ "application/json" })
	public List<hotels> countHotelSearch(hotels hh) {
		String h = "";
		String ci = "";
		String co = "";

		if (hh.getHotelName() == null) {
			h = "";
		} else {
			h = hh.getHotelName();
		}
		if (hh.getCountry() == null) {
			co = "";
		} else {
			co = hh.getCountry();
		}
		if (hh.getCity() == null) {
			ci = "";
		} else {
			ci = hh.getCity();
		}
		System.out.println("Lakhi Rajewalia");

		return vendor.countSearchHotels(h, ci, co);
	}

	@POST
	@Path("/getHotelsBySearch")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String advanceSearch(hotels hh) {
		String h = "";
		String ci = "";
		String co = "";
		String st = "";
		String rt = "";
		String zip = "";

		if (hh.getHotelName() == null) {
			h = "";
		} else {
			h = hh.getHotelName();
		}
		if (hh.getCountry() == null) {
			co = "";
		} else {
			co = hh.getCountry();
		}
		if (hh.getCity() == null) {
			ci = "";
		} else {
			ci = hh.getCity();
		}
		if(hh.getState() == null){
			st = "";
		} else {
			st = hh.getState();
		}
		if(hh.getRating() == "No Rating"){
			rt = "";
		} else {
			rt = hh.getRating();
		}
		if(hh.getZip() == null){
			zip = "";
		} else {
			zip = hh.getZip();
		}

		List<hotels> l = vendor.advanceSearch(h, ci, co, st, rt, zip);

		String json = new Gson().toJson(l);
		String z = json.toString();

		return z;
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String vLogout() {
		String z = (String) request.getSession().getAttribute("vmail");
		Integer logStatus = vendor.checkLoginStatusByEmail(z);
		vendor.updateLoginStatus(String.valueOf(logStatus));
		
		session = request.getSession(false);
		//s.invalidate();
		session.removeAttribute("vmail");

		Cookie cookie = null;
		Cookie[] ck = request.getCookies();
		for (int i = 0; i < ck.length; i++) {
			cookie = ck[i];
			cookie.setMaxAge(0);
		}
		return "{\"val\":\"logout\"}";
	}

}
