package com.lakhi.realestate.dbconfig;

import java.io.*;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class propertyConfig extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static Properties getLangProperties(HttpServletRequest request, HttpServletResponse response){
		Properties prop = new Properties();
		try {
			Cookie[] cookies = request.getCookies();
			Cookie cookie;
			if (cookies != null) {
				/* Cookie cookie1 = new Cookie("lang", lang);
				response.addCookie(cookie1);  */
				for (int i = 0; i < cookies.length; i++) {
				    cookie=cookies[i];
				    String zz="";
				    if (cookie.getName().equals("lang")) {
				    	if (cookie.getValue().equals("en")) {
							zz = "lang_en.properties";
						} else if (cookie.getValue().equals("fr")) {
							zz = "lang_fr.properties";
						} else if (cookie.getValue().equals("ch")) {
							//zz = "lang_ch.properties";
							FileInputStream input = new FileInputStream(new File("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/resources/lang_ch.properties"));
					    	prop.load(new InputStreamReader(input));
						} else if (cookie.getValue().equals("it")) {
							zz = "lang_it.properties";
						} else if (cookie.getValue().equals("nr")) {
							zz = "lang_nr.properties";
						} else if (cookie.getValue().equals("sp")) {
							zz = "lang_sp.properties";
						} else {
							zz = "lang_en.properties";
						}
				    	
				    	prop.load(new FileInputStream("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/resources/" + zz));
				    }
				}
			} else {
				Cookie cookie1 = new Cookie("lang", "en");
				response.addCookie(cookie1);
				
				FileInputStream input = new FileInputStream(new File("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/resources/lang_en.properties"));
		    	prop.load(new InputStreamReader(input));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	public static Properties getPayUProperty() throws FileNotFoundException, IOException{
		Properties prop = new Properties();
		
		FileInputStream input = new FileInputStream(new File("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/java/com/lakhi/realestate/dbconfig/payU.properties"));
    	prop.load(new InputStreamReader(input));
		
		return prop;
	}
	
	public static Properties getMailChimProperty() throws FileNotFoundException, IOException{
		Properties prop = new Properties();
		
		FileInputStream input = new FileInputStream(new File("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/java/com/lakhi/realestate/dbconfig/mandrill.properties"));
    	prop.load(new InputStreamReader(input));
		return prop;
	}
	
	public static Properties getSEOProperty() throws FileNotFoundException, IOException{
		Properties prop = new Properties();
		
		FileInputStream input = new FileInputStream(new File("/home/matrix/Documents/workspace-sts-3.8.3.RELEASE/realestate/src/main/resources/seo.properties"));
    	prop.load(new InputStreamReader(input));
		return prop;
	}
	
}

