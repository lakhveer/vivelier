package com.lakhi.realestate.dbconfig;

import java.io.*;
import java.net.URL;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.StringTokenizer;

import au.com.bytecode.opencsv.CSVReader;

public class DB {

	public static String backupDatabase(String fileLocation) {

		try {
			String dbName = "realestate";
			String dbUser = "root";
			String dbPass = "root";

			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			Date expdate = new Date();
			String date = dateFormat.format(expdate);

			String executeCmd = "";
			executeCmd = "mysqldump -u " + dbUser + " -p" + dbPass + " " + dbName + " -r " + fileLocation + "/" + date
					+ "-backup.sql";

			Process runtimeProcess = Runtime.getRuntime().exec(executeCmd);
			int processComplete = runtimeProcess.waitFor();
			if (processComplete == 0) {
				System.out.println("Backup taken successfully");
			} else {
				System.out.println("Could not take mysql backup");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Done";
	}

	public static String downloadFile(String fileLocation) throws IOException {

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date expdate = new Date();
		String date = dateFormat.format(expdate);

		String fileName = date + "-backup.sql"; // The file that will be saved
												// on your computer
		// URL link = new
		// URL("http://localhost:8080/realestate/DBbackup/"+fileName); //The
		// file that you want to download
		URL link = new URL(fileLocation + fileName); // The file that you want
														// to download

		// Code to download
		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();

		FileOutputStream fos = new FileOutputStream(fileName);
		fos.write(response);
		fos.close();
		// End download code

		System.out.println("Finished");

		return "" + link;
	}

}
