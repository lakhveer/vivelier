package com.lakhi.realestate.dbconfig;

import javax.xml.bind.DatatypeConverter;


public class secure {

	public static String decodeToken(String token) {
		
		 String decoded = new String(DatatypeConverter.parseBase64Binary(token));
//		 String[] a =  decoded.split("--");
//         System.out.println("decoded value is \t \ntoken = " + a[0]+"\nemail = " + a[1]+"\npassword = " + a[2]);

		return decoded;
	}

	public static String encodedToken(String token) {

		String encoded = DatatypeConverter.printBase64Binary(token.getBytes());
//        System.out.println("encoded value is \t" + encoded);
		
		return encoded;
	}

	
}
