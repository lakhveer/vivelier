package com.lakhi.realestate.dbconfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.MessageContent;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;

public class mendrillTemplate {
	
	public static void testTemplateMail() throws FileNotFoundException, IOException {
		Properties prop = propertyConfig.getMailChimProperty();
		MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password
		
	    try {
	        MandrillMessage message = new MandrillMessage();

	        //Set recipient
	        ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	        Recipient recipient = new Recipient();
	        recipient.setEmail("lakhveer@devs.matrixmarketers.com");
	        recipient.setName("LAkhveer");
	        
	        recipients.add(recipient);
	        message.setText("TEST");
	        message.setTo(recipients);
	        message.setPreserveRecipients(true);

	        
	        
	        
	        //Set global merge vars
	        List<MergeVar> globalMergeVars = new ArrayList<>();
	        MergeVar mergeVar = new MergeVar();
	        mergeVar.setName("Lakhveer Singh");
	        mergeVar.setContent("CONTENT LIST");
	        globalMergeVars.add(mergeVar);
	        message.setGlobalMergeVars(globalMergeVars);

	        //Set merge language (*important)
	        
	       //message.setMergeLanguage("handlebars");
	      
	        //You must provide at least an empty template content
	        Map<String, String> tc = new HashMap<>();
	        tc.put("description", "Description");
	       
	        
	        //Send mail
	        MandrillMessageStatus[] messageStatusReports =
	        		mandrillApi.messages().sendTemplate("LakhiTest", tc, message, false);
	        
	        if (messageStatusReports != null && messageStatusReports.length > 0) {
	           System.out.println("Mail sent info: " + messageStatusReports[0].getStatus());
	        }
	    } catch (MandrillApiError e) {
	        e.printStackTrace();
	    } 
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		testTemplateMail();
		System.out.println("DONE");
	}
	
	
}
