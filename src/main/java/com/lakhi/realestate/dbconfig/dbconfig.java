package com.lakhi.realestate.dbconfig;

import java.sql.*;

public class dbconfig {

	public static Connection getConnection() throws IllegalStateException {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/vivelier", "root", "root");
			// con =
//			 DriverManager.getConnection("jdbc:mysql://localhost:3306/vivelier","vivelier","Admin1!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static String getBaseURL(){
		return "http://localhost:8080/realestate/";
//		return "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/";
	}

}
