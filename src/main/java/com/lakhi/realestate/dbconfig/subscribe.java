package com.lakhi.realestate.dbconfig;

import com.ecwid.mailchimp.MailChimpClient;
import com.ecwid.mailchimp.MailChimpException;
import com.ecwid.mailchimp.method.v2_0.lists.*;
import java.io.*;
import java.util.Properties;

import org.json.JSONObject;

public class subscribe {

    private final static MailChimpClient mailChimpClient = new MailChimpClient();

    public static String subscribeMail(String email,String name) throws IOException, MailChimpException{
    	
    	Properties prop = propertyConfig.getMailChimProperty();
    	SubscribeMethod subscribeMethod = new SubscribeMethod();
        subscribeMethod.apikey = prop.getProperty("API_KEY");
        subscribeMethod.id = prop.getProperty("LIST_ID");
        subscribeMethod.email = new Email();
        subscribeMethod.email.email = email;
        
        mailChimpClient.execute(subscribeMethod);
        System.out.println("Done");
    	return "Done";
    }
   
}
