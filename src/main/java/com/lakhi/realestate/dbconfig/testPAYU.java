package com.lakhi.realestate.dbconfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import com.lakhi.realestate.vender.model.*;

public class testPAYU {

	private static Properties prop = new Properties();
	
	/*private static String mMerchantKey = "gtKFFx";
	private static String mSalt = "eCwWELxi";
	private static String mBaseURL = "https://test.payu.in";*/
	// private static String mBaseURL = "https://secure.payu.in";

	private static String mAction; // For Final URL
	private static String mTXNId; // This will create below randomly
	private static String mHash; // This will create below randomly
	private static String mProductInfo = "Test"; // Passing String only
	private static double mAmount; // From Previous Activity
	// private static String mSuccessUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/thankyou";
	// private static String mFailedUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/paymentfail";
	// private static String vSuccessUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/vendor/thankyou";
	// private static String vFailedUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/vendor/paymentfail";
	// private static String fSuccessUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/Vendors/thanks";
	// private static String fFailedUrl =
	// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/#/Vendors/paymenterror";

	private static String mSuccessUrl = dbconfig.getBaseURL() + "#/thankyou";
	private static String mFailedUrl = dbconfig.getBaseURL() + "#/paymentfail";

	private static String vSuccessUrl = dbconfig.getBaseURL() + "#/vendor/thankyou";
	private static String vFailedUrl = dbconfig.getBaseURL() + "#/vendor/paymentfail";

	private static String fSuccessUrl = dbconfig.getBaseURL() + "#/Vendors/thanks";
	private static String fFailedUrl = dbconfig.getBaseURL() + "#/Vendors/paymenterror";

	public static Map<String, String> payu(vendor v) throws FileNotFoundException, IOException {
		
		prop.load(new FileInputStream("/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/java/com/lakhi/realestate/dbconfig/payU.properties"));
		//prop.load(new FileInputStream("/var/lib/tomcat8/webapps/vivelier/WEB-INF/classes/com/lakhi/realestate/dbconfig/payU.properties"));
		String mMerchantKey = prop.getProperty("key");
		String mSalt = prop.getProperty("salt");
		String mBaseURL = prop.getProperty("baseURL");
		
		Random rand = new Random();
		String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);

		mTXNId = hashCal("SHA-256", randomString).substring(0, 20);
		mAmount = new BigDecimal(v.getAmount()).setScale(0, RoundingMode.UP).intValue();

		mHash = hashCal("SHA-512", mMerchantKey + "|" + mTXNId + "|" + mAmount + "|" + mProductInfo + "|"
				+ v.getFirst_name() + "|" + v.getEmail() + "|||||||||||" + mSalt);

		mAction = mBaseURL.concat("/_payment");

		Map<String, String> mapParams = new HashMap<>();

		mapParams.put("key", mMerchantKey);
		mapParams.put("txnid", mTXNId);
		mapParams.put("amount", String.valueOf(mAmount));
		mapParams.put("productinfo", mProductInfo);
		mapParams.put("firstname", v.getFirst_name());
		mapParams.put("email", v.getEmail());
		mapParams.put("phone", v.getPhone());
		mapParams.put("surl", mSuccessUrl);
		mapParams.put("furl", mFailedUrl);
		mapParams.put("hash", mHash);
		mapParams.put("lastname", v.getLast_name());
		// mapParams.put("service_provider", mServiceProvider);

		return mapParams;
	}

	public static Map<String, String> updatePlan(vendor v) throws FileNotFoundException, IOException {
		
		prop.load(new FileInputStream("/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/java/com/lakhi/realestate/dbconfig/payU.properties"));
		//prop.load(new FileInputStream("/var/lib/tomcat8/webapps/vivelier/WEB-INF/classes/com/lakhi/realestate/dbconfig/payU.properties"));
		String mMerchantKey = prop.getProperty("key");
		String mSalt = prop.getProperty("salt");
		String mBaseURL = prop.getProperty("baseURL");
		
		Random rand = new Random();
		String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);

		mTXNId = hashCal("SHA-256", randomString).substring(0, 20);
		mAmount = new BigDecimal(v.getAmount()).setScale(0, RoundingMode.UP).intValue();

		mHash = hashCal("SHA-512", mMerchantKey + "|" + mTXNId + "|" + mAmount + "|" + mProductInfo + "|"
				+ v.getFirst_name() + "|" + v.getEmail() + "|||||||||||" + mSalt);

		mAction = mBaseURL.concat("/_payment");

		Map<String, String> mapParams = new HashMap<>();

		mapParams.put("key", mMerchantKey);
		mapParams.put("txnid", mTXNId);
		mapParams.put("amount", String.valueOf(mAmount));
		mapParams.put("productinfo", mProductInfo);
		mapParams.put("firstname", v.getFirst_name());
		mapParams.put("email", v.getEmail());
		mapParams.put("phone", v.getPhone());
		mapParams.put("surl", vSuccessUrl);
		mapParams.put("furl", vFailedUrl);
		mapParams.put("hash", mHash);
		mapParams.put("lastname", v.getLast_name());
		// mapParams.put("service_provider", mServiceProvider);

		return mapParams;
	}

	public static Map<String, String> createNewListing(vendorListing v) throws FileNotFoundException, IOException {
		
		prop.load(new FileInputStream("/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/java/com/lakhi/realestate/dbconfig/payU.properties"));
		//prop.load(new FileInputStream("/var/lib/tomcat8/webapps/vivelier/WEB-INF/classes/com/lakhi/realestate/dbconfig/payU.properties"));
		String mMerchantKey = prop.getProperty("key");
		String mSalt = prop.getProperty("salt");
		String mBaseURL = prop.getProperty("baseURL");
		
		Random rand = new Random();
		String randomString = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);

		mTXNId = hashCal("SHA-256", randomString).substring(0, 20);
		mAmount = new BigDecimal(v.getSignupPlan()).setScale(0, RoundingMode.UP).intValue();

		mHash = hashCal("SHA-512", mMerchantKey + "|" + mTXNId + "|" + mAmount + "|" + mProductInfo + "|"
				+ v.getContactPerson() + "|" + v.getEmail() + "|||||||||||" + mSalt);

		mAction = mBaseURL.concat("/_payment");

		Map<String, String> mapParams = new HashMap<>();

		mapParams.put("key", mMerchantKey);
		mapParams.put("txnid", mTXNId);
		mapParams.put("amount", String.valueOf(mAmount));
		mapParams.put("productinfo", mProductInfo);
		mapParams.put("firstname", v.getContactPerson());
		mapParams.put("email", v.getEmail());
		mapParams.put("phone", v.getPhone());
		mapParams.put("surl", fSuccessUrl);
		mapParams.put("furl", fFailedUrl);
		mapParams.put("hash", mHash);
		mapParams.put("salt", mSalt);
		// mapParams.put("service_provider", mServiceProvider);

		return mapParams;
	}

	private static String hashCal(String type, String str) {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append("0");
				}
				hexString.append(hex);
			}

		} catch (Exception e) {
		}

		return hexString.toString();
	}

}
