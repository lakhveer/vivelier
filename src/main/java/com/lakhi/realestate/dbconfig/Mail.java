package com.lakhi.realestate.dbconfig;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import com.lakhi.realestate.vender.model.vendorContact;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;

public class Mail {
	
	public static String send(String email, String firstName, String pwd) throws MandrillApiError, IOException{
		Properties prop = propertyConfig.getMailChimProperty();
		
		MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

		String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
        		+ "<p style='margin-left:10px;'> Dear " + firstName +"</p>"
        				+ "<p style='margin-left:15px;'> Your password is changed. Now new password is <b>"+pwd+"</b></p>"
        		+ "</div>";
		
		// create your message
		MandrillMessage message = new MandrillMessage();
		message.setSubject("Reset Password");
		message.setHtml(""+msg);
		message.setAutoText(true);
		message.setFromEmail(prop.getProperty("mandrillEmail"));
		message.setFromName(prop.getProperty("mandrillFromName"));
		
		// add recipients
		ArrayList<Recipient> recipients = new ArrayList<Recipient>();
		Recipient recipient = new Recipient();
		
		recipient = new Recipient();
		recipient.setEmail(email);
		recipients.add(recipient);
		message.setTo(recipients);
		message.setPreserveRecipients(true);
		
		ArrayList<String> tags = new ArrayList<String>();
//		tags.add("test");
//		tags.add("helloworld");
		message.setTags(tags);
		// ... add more message details if you want to!
		// then ... send
		MandrillMessageStatus[] messageStatusReports = mandrillApi
		        .messages().send(message, false);
		
		return "Done...";
	}
	
public static String signup(String fname, String email,String pwd) throws MandrillApiError, IOException{
		
	Properties prop = propertyConfig.getMailChimProperty();
		MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

		String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
        		+ "<p style='margin-left:10px;'> Dear " + fname +"</p>"
        				+ "<p style='margin-left:15px;'> Thankyou for signup.<br>Please Login by email : <b>"+email+"</b><br> and password is <b>"+fname+"</b></p>"
        		+ "</div>";
		
		// create your message
		MandrillMessage message = new MandrillMessage();
		message.setSubject("Login Password");
		message.setHtml(""+msg);
		message.setAutoText(true);
		message.setFromEmail(prop.getProperty("mandrillEmail"));
		message.setFromName(prop.getProperty("mandrillFromName"));
		
		// add recipients
		ArrayList<Recipient> recipients = new ArrayList<Recipient>();
		Recipient recipient = new Recipient();
		
		recipient = new Recipient();
		recipient.setEmail(email);
		recipients.add(recipient);
		message.setTo(recipients);
		message.setPreserveRecipients(true);
		
		ArrayList<String> tags = new ArrayList<String>();
//		tags.add("test");
//		tags.add("helloworld");
		message.setTags(tags);
		// ... add more message details if you want to!
		// then ... send
		MandrillMessageStatus[] messageStatusReports = mandrillApi
		        .messages().send(message, false);
		
		return "Done...";
	}

public static String updateVendorPlanMail(String email, String firstName, String plan) throws MandrillApiError, IOException{
	
	Properties prop = propertyConfig.getMailChimProperty();
	MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

	String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
    		+ "<p style='margin-left:10px;'> Dear " + firstName +"</p>"
    				+ "<p style='margin-left:15px;'> Thanks for update your plan. Now your new plan is <b>"+plan+"</b>.</p>"
    		+ "</div>";
	
	// create your message
	MandrillMessage message = new MandrillMessage();
	message.setSubject("Update Plan");
	message.setHtml(""+msg);
	message.setAutoText(true);
	message.setFromEmail(prop.getProperty("mandrillEmail"));
	message.setFromName(prop.getProperty("mandrillFromName"));
	
	// add recipients
	ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	Recipient recipient = new Recipient();
	
	recipient = new Recipient();
	recipient.setEmail(email);
	recipients.add(recipient);
	message.setTo(recipients);
	message.setPreserveRecipients(true);
	
	ArrayList<String> tags = new ArrayList<String>();
//	tags.add("test");
//	tags.add("helloworld");
	message.setTags(tags);
	// ... add more message details if you want to!
	// then ... send
	MandrillMessageStatus[] messageStatusReports = mandrillApi
	        .messages().send(message, false);
	
	return "Done...";
}

public static String createList(String email, String firstName, String plan) throws MandrillApiError, IOException{
	
	Properties prop = propertyConfig.getMailChimProperty();
	MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

	String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
    		+ "<p style='margin-left:10px;'> Dear " + firstName +"</p>"
    				+ "<p style='margin-left:15px;'> Thanks for Create List. Now your new plan is <b>"+plan+" yr</b>."
    						+ "Ans your Login credentials are : <br>"
    						+ "Email : <b>"+email+"</b> <br>"
    								+ "password : <b>p@$$word</b> </p>"
    		+ "</div>";
	
	// create your message
	MandrillMessage message = new MandrillMessage();
	message.setSubject("Create List");
	message.setHtml(""+msg);
	message.setAutoText(true);
	message.setFromEmail(prop.getProperty("mandrillEmail"));
	message.setFromName(prop.getProperty("mandrillFromName"));
	
	// add recipients
	ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	Recipient recipient = new Recipient();
	
	recipient = new Recipient();
	recipient.setEmail(email);
	recipients.add(recipient);
	message.setTo(recipients);
	message.setPreserveRecipients(true);
	
	ArrayList<String> tags = new ArrayList<String>();
//	tags.add("test");
//	tags.add("helloworld");
	message.setTags(tags);
	// ... add more message details if you want to!
	// then ... send
	MandrillMessageStatus[] messageStatusReports = mandrillApi
	        .messages().send(message, false);
	
	return "Done...";
}

public static String contactVendor(vendorContact vc) throws MandrillApiError, IOException{
	
	Properties prop = propertyConfig.getMailChimProperty();
	MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

	String msg = "<div style='border:1px solid #f1f1f1;'>"
    		+ "<p> Mrs/Miss <b>"+vc.getName()+"</b> contact you.</p>"
    				+ "<table style='padding:5px;border:2px solid #f3f3f3;'>"
    				+ "<tr>"
    				+ "<td style='padding:5px;'><label>Title</label></td>"
    				+ "<td style='padding:5px;'><span>"+vc.getTitle()+"</span></td>"
    				+ "</tr>"
    				+ "<tr>"
    				+ "<td style='padding:5px;'><label>Description</label></td>"
    				+ "<td style='padding:5px;'><span>"+vc.getDescription()+"</span></td>"
    				+ "</tr>"
    				+ "</table>"
    		+ "</div>";
	
	// create your message
	MandrillMessage message = new MandrillMessage();
	message.setSubject("Contact Vendor");
	message.setHtml(""+msg);
	message.setAutoText(true);
	message.setFromEmail(prop.getProperty("mandrillEmail"));
	message.setFromName(prop.getProperty("mandrillFromName"));
	
	// add recipients
	ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	Recipient recipient = new Recipient();
	
	recipient = new Recipient();
	recipient.setEmail(vc.getvEmail());
	recipients.add(recipient);
	message.setTo(recipients);
	message.setPreserveRecipients(true);
	
	ArrayList<String> tags = new ArrayList<String>();
//	tags.add("test");
//	tags.add("helloworld");
	message.setTags(tags);
	// ... add more message details if you want to!
	// then ... send
	MandrillMessageStatus[] messageStatusReports = mandrillApi
	        .messages().send(message, false);
	
	return "Done...";
}

public static String forgotVendorPassword(String email, String password) throws MandrillApiError, IOException{
	
	Properties prop = propertyConfig.getMailChimProperty();
	MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

	String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
    		+ "<p style='margin-left:10px;'> Dear " + email +"</p>"
    				+ "<p style='margin-left:15px;'> Your password is successfully reset. Now your new password is <b>"+password+"</b>.</p>"
    		+ "</div>";
	// create your message
	MandrillMessage message = new MandrillMessage();
	message.setSubject("Reset Password");
	message.setHtml(""+msg);
	message.setAutoText(true);
	message.setFromEmail(prop.getProperty("mandrillEmail"));
	message.setFromName(prop.getProperty("mandrillFromName"));
	// add recipients
	ArrayList<Recipient> recipients = new ArrayList<Recipient>();
	Recipient recipient = new Recipient();
	recipient = new Recipient();
	recipient.setEmail(email);
	recipients.add(recipient);
	message.setTo(recipients);
	message.setPreserveRecipients(true);
	ArrayList<String> tags = new ArrayList<String>();
	message.setTags(tags);
	// ... add more message details if you want to!  then ... send
	MandrillMessageStatus[] messageStatusReports = mandrillApi
	        .messages().send(message, false);
	return "Done...";
}

	public static String contactUsMail(vendorContact vc) throws FileNotFoundException, IOException, MandrillApiError{
		
		Properties prop = propertyConfig.getMailChimProperty();
		MandrillApi mandrillApi = new MandrillApi(prop.getProperty("mandrillApi")); //password

		String msg = "<div style='border:1px solid #f1f1f1; width:50%;'>"
	    		+ "<p style='margin-left:10px;'> Dear Admin</p>"
	    				+ "<p style='margin-left:15px;'> Mr/Miss "+vc.getFirstName()+" "+vc.getLastName()+" contact you.</p>"
	    		+ "</div>";
		// create your message
		MandrillMessage message = new MandrillMessage();
		message.setSubject("Contact Us");
		message.setHtml(""+msg);
		message.setAutoText(true);
		message.setFromEmail(prop.getProperty("mandrillEmail"));
		message.setFromName(prop.getProperty("mandrillFromName"));
		// add recipients
		ArrayList<Recipient> recipients = new ArrayList<Recipient>();
		Recipient recipient = new Recipient();
		recipient = new Recipient();
		recipient.setEmail(prop.getProperty("adminMail"));
		recipients.add(recipient);
		message.setTo(recipients);
		message.setPreserveRecipients(true);
		ArrayList<String> tags = new ArrayList<String>();
		message.setTags(tags);
		// ... add more message details if you want to!  then ... send
		MandrillMessageStatus[] messageStatusReports = mandrillApi
		        .messages().send(message, false);
		return "Done";
	}

}
