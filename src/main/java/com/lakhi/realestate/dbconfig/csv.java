package com.lakhi.realestate.dbconfig;

import java.io.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.lakhi.realestate.admin.model.hotels;

public class csv {

	public static void main(String[] args) {
		// String file = "/home/matrix/test.csv";
		// uploadCSVFile(file);
	}

	public static String uploadCSVFile(File file) {

		try {
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String s;
			String z[];
			while ((s = br.readLine()) != null) {
				z = s.split(",");
				System.out.print(z[0] + "\t");
				System.out.print(z[1] + "\t");
				System.out.print(z[2] + "\t");
				System.out.print(z[3] + "\t");
				System.out.print(z[4] + "\t");
				System.out.print(z[5] + "\t");
				System.out.println(z[6].replaceAll("\\W ", " ") + "\t");
				System.out.print(z[7] + "\t");
				System.out.print(z[8] + "\t");
				System.out.print(z[9] + "\n");

				Connection con = dbconfig.getConnection();
				Statement st = con.createStatement();

				String q = "insert into hotels values(null,'" + z[1] + "','" + z[2] + "','" + z[3] + "','" + z[4]
						+ "','" + z[5] + "','" + z[6] + "'" + ",'" + z[7] + "','" + z[8] + "','" + z[9] + "') ";
				st.execute(q);

			}
			fr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Done";
	}

	public static String createCSVFile(String type, List<hotels> l, String path) {

		// String path = "/var/lib/tomcat8/webapps/vivelier/DBbackup/";
		// String path =
		// "/home/matrix/Documents/workspace-sts-3.8.1.RELEASE/realestate/src/main/webapp/DBbackup/";

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date expdate = new Date();
		String date = dateFormat.format(expdate);

		String fileName = type.replaceAll(" ", "-") + "-" + date + ".csv";
		// System.out.println("LAkhi = " + path + "/" + fileName);
		try {
			File file = new File(path + "/" + fileName);
			file.createNewFile();
			FileWriter writer = new FileWriter(file);
			writer.write("S.No,");
			writer.write("HOTELNAME,");
			writer.write("RATING,");
			writer.write("WEBSITE,");
			writer.write("EMAIL1,");
			writer.write("EMAIL2,");
			writer.write("STREET ADDRESS,");
			writer.write("CITY,");
			writer.write("STATE,");
			writer.write("COUNTRY,");
			writer.write("ZIP CODE,");
			writer.write("TELEPHONE1,");
			writer.write("TELEPHONE2,");
			writer.write("REMARKS,\n");
			int i = 1;
			for (hotels h : l) {
				writer.write(i + ",");
				writer.write(h.getHotelName().replaceAll("\\W", " ") + ",");
				writer.write(h.getRating() + ",");
				writer.write(h.getWebsite() + ",");
				writer.write(h.getEmail1() + ",");
				writer.write(h.getEmail2() + ",");
				writer.write(h.getAddress().replaceAll("\\W", " ") + ",");
				writer.write(h.getCity() + ",");
				writer.write(h.getState() + ",");
				writer.write(h.getCountry() + ",");
				writer.write(h.getZip() + ",");
				writer.write(h.getTelephone1() + ",");
				writer.write(h.getTelephone2() + ",");
				writer.write(h.getRemarks() + ",\n");
				i++;
			}
			writer.flush();
			writer.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "" + fileName;
	}

	public static String downloadCSVFile(String type, List<hotels> l, String path) throws IOException {

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date expdate = new Date();
		String date = dateFormat.format(expdate);

		String fileName = type + "-" + date + ".csv";

		// URL link = new URL(
		// "http://ec2-52-66-150-60.ap-south-1.compute.amazonaws.com:8080/vivelier/DBbackup/"
		// + fileName);

//		URL link = new URL("http://localhost:8080/realestate/DBbackup/" + fileName);
		URL link = new URL(dbconfig.getBaseURL()+"DBbackup/" + fileName);

		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[1024];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();
		FileOutputStream fos = new FileOutputStream(fileName);
		fos.write(response);
		fos.close();
		// End download code

		System.out.println("Finished");

		return "" + link;
	}

}
