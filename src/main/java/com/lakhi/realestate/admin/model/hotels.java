package com.lakhi.realestate.admin.model;

import java.io.Serializable;

public class hotels implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String hotelName;
	private String website;
	private String email1;
	private String email2;
	private String address;
	private String city;
	private String country;
	private String telephone1;
	private String telephone2;
	private String images;
	private String description;
	private String url;
	private String rating;
	private String state;
	private String zip;
	private String remarks;
	

	public hotels() {

	}

	public hotels(int id, String hotelName, String website, String email1, String email2, String address, String city,
			String country, String telephone1, String telephone2, String images, String description, String url,
			String rating, String state, String zip, String remarks) {
		super();
		this.id = id;
		this.hotelName = hotelName;
		this.website = website;
		this.email1 = email1;
		this.email2 = email2;
		this.address = address;
		this.city = city;
		this.country = country;
		this.telephone1 = telephone1;
		this.telephone2 = telephone2;
		this.images = images;
		this.description = description;
		this.url = url;
		this.rating = rating;
		this.state = state;
		this.zip = zip;
		this.remarks = remarks;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return (email2 == null ? "" : email2);
		
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelephone1() {
		return telephone1;
	}

	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}

	public String getTelephone2() {
		
		return ((telephone2 == null || telephone2.isEmpty()) ? "" : telephone2);
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRating() {
		return ((rating == null || rating.isEmpty()) ? "No Rating" : rating);
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getRemarks() {
		return ((remarks == null || remarks.isEmpty()) ? "" : remarks );
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
