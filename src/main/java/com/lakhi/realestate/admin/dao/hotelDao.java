package com.lakhi.realestate.admin.dao;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;
import com.lakhi.realestate.admin.model.hotels;
import com.lakhi.realestate.admin.service.hotelService;
import com.lakhi.realestate.dbconfig.dbconfig;

public class hotelDao implements hotelService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs;

	@Override
	public List<hotels> getHotelByCity(String city) {
		List<hotels> l = new ArrayList<hotels>();
		try {

			st = con.createStatement();
			String query = " select * from hotelData where city = '" + city.replaceAll("-", " ") + "' ";
			System.out.println(query);
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);
				// System.out.println(h.getHotelName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> getHotelByState(String state) {
		List<hotels> l = new ArrayList<hotels>();
		try {

			st = con.createStatement();
			String query = " select * from hotelData where COUNTRY = '" + state + "' ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				//String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);
				// System.out.println(h.getHotelName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}
	
	@Override
	public List<hotels> getHotelByCountry(String country) {
		List<hotels> l = new ArrayList<hotels>();
		try {

			st = con.createStatement();
			String query = " select * from hotelData where COUNTRY = '" + country.replaceAll("-", " ") + "' ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);
				// System.out.println(h.getHotelName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> totalHotels()  {
		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
			String query = " select COUNT(id) from hotelData";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int i = rs.getInt(1);
				hotels h = new hotels();
				h.setId(i);

				l.add(h);

				// System.out.println("DATA = " + i);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> getAllHotels(int limit) {

		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
			//String query = " select * from hotels order by SNo desc limit 10 OFFSET " + limit;
			String query = " select * from hotelData order by id desc limit 10 OFFSET " + limit;
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country);
				h.setDescription(remarks);

				l.add(h);

				// System.out.println(h.getAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;

	}

	@Override
	public List<String> getCity() {
		List<String> l = new ArrayList<String>();

		try {

			st = con.createStatement();
			String query = " select DISTINCT CITY,COUNTRY from hotelData ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String city = rs.getString("CITY");
				String country = rs.getString("COUNTRY");
				String a = "(" + country + ") " + city;
				l.add(a);
				//System.out.println("data = " + country);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<String> getState(String state) {
		List<String> ll = new ArrayList<String>();
		try {
			st = con.createStatement();
			String query = " select DISTINCT state state,COUNTRY from hotelData ";
			System.out.println(query);
			rs = st.executeQuery(query);
			while (rs.next()) {
				String city = rs.getString("state");
				String country = rs.getString("COUNTRY");
				String a = "(" + country + ") " + city;
				ll.add(a);
				// System.out.println("data = " + country);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return ll;
	}
	
	@Override
	public List<String> getCountries() {
		List<String> l = new ArrayList<String>();

		try {

			st = con.createStatement();
			String query = " select DISTINCT COUNTRY from hotelData ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String country = rs.getString("COUNTRY");
				l.add(country);

				// System.out.println("data = " + country);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> getHotelById(int id) {
		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
			String query = " select * from hotelData where id = " + id;
			rs = st.executeQuery(query);
			while (rs.next()) {
				//int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);

				// System.out.println(h.getAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void updateHotel(hotels h) {

		try {

			st = con.createStatement();
//			String query = "update hotels set HOTELNAMES='" + h.getHotelName() + "', CITY='" + h.getCity()
//					+ "',WEBSITE='" + h.getWebsite() + "'" + ",EMAIL1='" + h.getEmail1() + "',EMAIL2='" + h.getEmail2()
//					+ "',ADDRESS='" + h.getAddress() + "',TELEPHONE1='" + h.getTelephone1() + "'," + "TELEPHONE2='"
//					+ h.getTelephone2() + "',COUNTRY='" + h.getCountry() + "', DESCRIPTION='"+h.getDescription()+"' where SNo = " + h.getId();
			String query = "update hotelData set hotelName='"+h.getHotelName()+"', rating='"+h.getRating()+"', url='"+h.getWebsite()+"',"
					+ "email1='"+h.getEmail1()+"', email2='"+h.getEmail2()+"', street='"+h.getAddress()+"', city='"+h.getCity()+"', state='"+h.getState()+"',"
							+ "country='"+h.getCountry()+"', zip='"+h.getZip()+"', phone1='"+h.getTelephone1()+"', phone2='"+h.getTelephone2()+"', remarks='"+h.getRemarks()+"' where id = " + h.getId();
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void addNewHotel(hotels h) {
		try {
			st = con.createStatement();
//			String query = " insert into hotels values (null,'" + h.getHotelName() + "','" + h.getCity() + "'," + "'"
//					+ h.getWebsite() + "','" + h.getEmail1() + "','" + h.getEmail2() + "','" + h.getAddress() + "',"
//					+ "'" + h.getTelephone1() + "','" + h.getTelephone2() + "','" + h.getCountry() + "','"+h.getDescription()+"' ) ";
			String query = "insert into hotelData values (null, '"+h.getHotelName()+"', '"+h.getRating()+"', '"+h.getWebsite()+"', '"+h.getEmail1()+"'"
					+ ", '"+h.getEmail2()+"', '"+h.getAddress()+"', '"+h.getCity()+"', '"+h.getState()+"', '"+h.getCountry()+"', '"+h.getZip()+"'"
							+ ", '"+h.getTelephone1()+"', '"+h.getTelephone2()+"', '"+h.getRemarks()+"' ) ";
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void addHotelGallery(hotels h) {

		try {

			st = con.createStatement();
			String query = " insert into hotelGallery values (null,'" + h.getId() + "','" + h.getImages() + "','"
					+ h.getUrl() + "' ) ";
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public List<hotels> getHotelGallery(int hotelId) {
		List<hotels> l = new ArrayList<hotels>();
		try {
			st = con.createStatement();
			String query = " select * from hotelGallery where hotel_id = " + hotelId;
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String images = rs.getString(3);
				String url = rs.getString(4);

				hotels h = new hotels();
				h.setId(id);
				h.setImages(images);
				h.setUrl(url);
				l.add(h);
				// System.out.println("Hotel id = " + h.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public List<hotels> allHotelByCountry(int limit, String country) {
		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
//			String query = " select * from hotelData where (country like '%" + country + "%') or (city like'%" + country
//					+ "%') or (hotelName like '%" + country + "%') limit 10 offset " + limit;
			String query = " select * from hotelData where (country like '%" + country + "%') or (city like'%" + country
					+ "%') or (hotelName like '%" + country + "%') limit 10 offset " + limit;
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);

				l.add(h);

				// System.out.println(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> totalHotelsBySearch(String data) {
		List<hotels> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = " select COUNT(id) from hotelData where (country like '%" + data + "%') or (city like'%" + data
					+ "%') or (hotelName like '%" + data + "%')";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int i = rs.getInt(1);
				hotels h = new hotels();
				h.setId(i);

				l.add(h);

				// System.out.println("DATA = " + query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> getAllHotels() {
		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
			String query = " select * from hotels " ;
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(3);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7);
				String telephone1 = rs.getString(8);
				String telephone2 = rs.getString(9);
				String country = rs.getString(10);
				String desc = rs.getString(11);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country);
				h.setDescription(desc);

				l.add(h);

				// System.out.println(h.getAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void deleteHotel(int id) {
		try {
			st = con.createStatement();
			String query = "delete from hotelData where id = " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
