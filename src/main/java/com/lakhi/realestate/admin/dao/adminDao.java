package com.lakhi.realestate.admin.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lakhi.realestate.admin.model.login;
import com.lakhi.realestate.admin.service.adminService;
import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.vender.model.vendor;

public class adminDao implements adminService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs;

	@Override
	public List<login> getProfile(String email) throws IllegalStateException {

		List<login> list = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select * from adminLogin where email = '" + email + "' ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String e = rs.getString(2);
				String firstName = rs.getString(4);
				String lastName = rs.getString(5);
				String address = rs.getString(6);
				String phone = rs.getString(7);
				String img = rs.getString(8);
				String date = rs.getString(9);

				String z[] = date.split("-");
				String d = z[2] + "-" + z[1] + "-" + z[0];
				
				login l = new login();
				l.setId(id);
				l.setEmail(e);
				l.setFirstName(firstName);
				l.setLastName(lastName);
				l.setAddress(address);
				l.setPhone(phone);
				l.setImg(img);
				l.setDate(d);
				
				list.add(l);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return list;
	}

	@Override
	public login updateProfile(login l) throws IllegalStateException {

		try {
			st = con.createStatement();
			String query = "update adminLogin set password = MD5('" + l.getPassword() + "'), " + "firstName = '"
					+ l.getFirstName() + "', lastName = '" + l.getLastName() + "'," + "address = '" + l.getAddress()
					+ "', phone = '" + l.getPhone() + "' where id = " + l.getId();
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendor> getAllVendors() {

		List<vendor> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select * from vendors";
			rs = st.executeQuery(query);
			while (rs.next()) {

				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String email = rs.getString(4);
				String phone = rs.getString(6);
				String plane = rs.getString(7);
				String img = rs.getString(8);
				String address = rs.getString(9);
				int status = rs.getInt(10);
				String date = rs.getString(11);
				String expDate = rs.getString(12);
				String payment = rs.getString(13);

				String z[] = date.split("-");
				String d = z[2] + "-" + z[1] + "-" + z[0];

				String z1[] = expDate.split("-");
				String d1 = z1[2] + "-" + z1[1] + "-" + z1[0];

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date expdate = new Date();
				String edate = dateFormat.format(expdate);
				
				vendor v = new vendor();
				v.setId(id);
				v.setFirst_name(firstName);
				v.setLast_name(lastName);
				v.setEmail(email);
				v.setPhone(phone);
				v.setAmount(plane);
				v.setImage(img);
				v.setAddress(address);
				v.setStatus(status);
				v.setDate(d);
				v.setExpireDate(d1);
				v.setExpDate(edate);
				v.setPayment(payment);

				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendor> getVendorById(int id) {
		List<vendor> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select * from vendors where vendor_id = " + id +" ";
			rs = st.executeQuery(query);
			while (rs.next()) {

				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String email = rs.getString(4);
				String phone = rs.getString(6);
				String plane = rs.getString(7);
				String img = rs.getString(8);
				String address = rs.getString(9);
				int status = rs.getInt(10);
				String date = rs.getString(11);
				String expDate = rs.getString(12);
				String payment = rs.getString(13);

				String z[] = date.split("-");
				String d = z[2] + "-" + z[1] + "-" + z[0];

				String z1[] = expDate.split("-");
				String d1 = z1[2] + "-" + z1[1] + "-" + z1[0];
				
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date expdate = new Date();
				String edate = dateFormat.format(expdate);

				int a = Integer.parseInt(z[2]+z[1]+z[0]);
				int b = Integer.parseInt(z1[0]+z1[1]+ z1[2]);
				
				DateFormat dd = new SimpleDateFormat("ddMMyyyy");
				int curDate = Integer.parseInt(dd.format(expdate));
				
				
				DateFormat dFormat = new SimpleDateFormat("yyyyMMdd");
				Date currentdate = new Date();
				System.out.println(dFormat.format(currentdate)+"\n"+b);
				int curdate1 = Integer.parseInt(dFormat.format(currentdate));
				
				String vendorStatus = "";
				if(curdate1 < b){
					vendorStatus = "Active";
					//System.out.println("A " + vendorStatus);
				} else {
					vendorStatus = "Expire";
					//System.out.println("B " + vendorStatus);
				}
								
				vendor v = new vendor();
				v.setId(id);
				v.setFirst_name(firstName);
				v.setLast_name(lastName);
				v.setEmail(email);
				v.setPhone(phone);
				v.setAmount(plane);
				v.setImage(img);
				v.setAddress(address);
				v.setStatus(status);
				v.setDate(d);
				v.setExpireDate(d1);
				v.setExpDate(edate);
				v.setPayment(payment);
				v.setVendorPaymentStatus(vendorStatus);
				
				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void resetVendorPassword(vendor v) {

		try {
			st = con.createStatement();
			String query = "update vendors set vendor_password = MD5('" + v.getPassword() + "') where vendor_id = "
					+ v.getId();
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public login updateProfilePic(login l) {
		try {
			st = con.createStatement();
			String query = "update adminLogin set image = '" + l.getImg() + "' where email = '" + l.getEmail() + "' ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void deactiveVendorAccount(int id) {
		try {
			st = con.createStatement();
			String query = "update vendors set vendor_status = '0'  where vendor_id = " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void activeVendorAccount(int id) {
		try {
			st = con.createStatement();
			String query = "update vendors set vendor_status = '1'  where vendor_id = " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<vendor> countVendors() {
		List<vendor> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = " select COUNT(vendor_id) from vendors ";
			rs = st.executeQuery(query);
			while(rs.next()){
				int id = rs.getInt(1);
				
				vendor v = new vendor();
				v.setId(id);
				
				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public List<vendor> getVendorsByPlan(String plan) {
		List<vendor> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select * from vendors where vendor_plan like '"+plan+"%'";
			rs = st.executeQuery(query);
			while (rs.next()) {

				int id = rs.getInt(1);
				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String email = rs.getString(4);
				String phone = rs.getString(6);
				String plane = rs.getString(7);
				String img = rs.getString(8);
				String address = rs.getString(9);
				int status = rs.getInt(10);
				String date = rs.getString(11);
				String expDate = rs.getString(12);
				String payment = rs.getString(13);

				String z[] = date.split("-");
				String d = z[2] + "-" + z[1] + "-" + z[0];

				String z1[] = expDate.split("-");
				String d1 = z1[2] + "-" + z1[1] + "-" + z1[0];

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date expdate = new Date();
				String edate = dateFormat.format(expdate);
				
				vendor v = new vendor();
				v.setId(id);
				v.setFirst_name(firstName);
				v.setLast_name(lastName);
				v.setEmail(email);
				v.setPhone(phone);
				v.setAmount(plane);
				v.setImage(img);
				v.setAddress(address);
				v.setStatus(status);
				v.setDate(d);
				v.setExpireDate(d1);
				v.setExpDate(edate);
				v.setPayment(payment);

				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public void updateAboutUs(String data) {
		try {
			st = con.createStatement();
			String query = "update about set content = '"+data+"'";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public String getAboutData() {
		String data = null;
		try {
			st = con.createStatement();
			String query = "select * from about where id = " + 1;
			rs = st.executeQuery(query);
			while(rs.next()){
				data = rs.getString("content");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public void updatePrivacy(String data) {
		try {
			st = con.createStatement();
			String query = "update privacy set content = '"+data+"'";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getPrivacyData() {
		String data = null;
		try {
			st = con.createStatement();
			String query = "select * from privacy where id = " + 1;
			rs = st.executeQuery(query);
			while(rs.next()){
				data = rs.getString("content");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public void updateTermsConditions(String data) {
		try {
			st = con.createStatement();
			String query = "update termsconditions set content = '"+data+"'";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getTermsConditions() {
		String data = null;
		try {
			st = con.createStatement();
			String query = "select * from termsconditions where id = " + 1;
			rs = st.executeQuery(query);
			while(rs.next()){
				data = rs.getString("content");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	@Override
	public void updateServices(String data) {
		try {
			st = con.createStatement();
			String query = "update services set content = '"+data+"'";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getServicesData() {
		String data = null;
		try {
			st = con.createStatement();
			String query = "select * from services where id = " + 1;
			rs = st.executeQuery(query);
			while(rs.next()){
				data = rs.getString("content");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

}
