package com.lakhi.realestate.admin.service;

import java.util.List;

import com.lakhi.realestate.admin.model.hotels;

public interface hotelService {

	public List<String> getCity();

	public List<String> getCountries();
	
	public List<String> getState(String state);

	public List<hotels> getHotelByCity(String city);

	public List<hotels> getHotelByCountry(String country);
	
	public List<hotels> getHotelByState(String state);
	
	public List<hotels> getAllHotels(int limit);
	
	public List<hotels> getHotelById(int id);
	
	public void updateHotel(hotels h);
	
	public List<hotels> totalHotels();
	
	public void addNewHotel(hotels h);
	
	public void addHotelGallery(hotels h);

	public List<hotels> getHotelGallery(int hotelId);
	
	public List<hotels> allHotelByCountry(int limit, String country);
	
	public List<hotels> totalHotelsBySearch(String data);
	
	public List<hotels> getAllHotels();
	
	public void deleteHotel(int id);
}
