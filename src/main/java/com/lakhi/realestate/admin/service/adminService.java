package com.lakhi.realestate.admin.service;

import java.util.List;

import com.lakhi.realestate.admin.model.login;
import com.lakhi.realestate.vender.model.vendor;

public interface adminService {

	public List<login> getProfile(String email);
	
	public login updateProfile(login l);
	
	public login updateProfilePic(login l);
	
	public List<vendor> getAllVendors();
	
	public List<vendor> getVendorsByPlan(String plan);
	
	public List<vendor> getVendorById(int id);
	
	public void resetVendorPassword(vendor v);
	
	public void deactiveVendorAccount(int id);
	
	public void activeVendorAccount(int id);
	
	public List<vendor> countVendors();
	
	public void updateAboutUs(String data);
	
	public String getAboutData();
	
	public void updatePrivacy(String data);
	
	public String getPrivacyData();
	
	public void updateTermsConditions(String data);
	
	public String getTermsConditions();
	
	public void updateServices(String data);
	
	public String getServicesData();
	
}
