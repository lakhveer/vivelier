package com.lakhi.realestate.vender.service;

import java.util.List;

import com.lakhi.realestate.vender.model.listCategory;
import com.lakhi.realestate.vender.model.vendorListing;

public interface ListingService {

	public void createListing(vendorListing l);
	
	public void createNewList(vendorListing l);
	
	public void addListingInVendor(vendorListing v);
	
	public List<String> getAllCountries();
	
	public List<listCategory> getMainCategories();
	
	public int getMainCategorieByName(String name);
	
	public List<listCategory> getSubCategories(String name);
	
	public List<vendorListing> getAllListing(String email);
	
	public List<vendorListing> getListById(int id);
	
	public void ListUpdate(vendorListing v);
	
	public void updateCoverImg(int id, String file);
	
	public List<vendorListing> getAllList();
	
	public List<listCategory> getCategories();
	
	public List<vendorListing> getSingleList(String name);
	
	public List<vendorListing> ListingData(String mid, String sid);
	
	public List<vendorListing> ListingByState(String mid, String sid, String st);

	public List<vendorListing> ListingByCountry(String mid, String sid, String st, String co);
	
	public List<vendorListing> mainListingData(String mid);
	
	public List<vendorListing> getListingDataByAddress(String data);
	
	public void deleteListing(int id);
	
	public List<vendorListing> getListAddress();

	public List<vendorListing> getListCity();
	
	public List<vendorListing> getListState();
	
	public List<vendorListing> getListCountry();
	
	public List<vendorListing> keywordSearch(String keywords);

	public List<vendorListing> vListkeywordSearch(String keywords, String email);
}
