package com.lakhi.realestate.vender.service;

import java.util.List;

import com.lakhi.realestate.vender.model.rating;
import com.lakhi.realestate.vender.model.vendorContact;

public interface contactService {

	public void contactVendor(vendorContact vc);
	
	public void addRating(rating r);
	
	public List<rating> getRatingByList(String name);
	
	public void contactUs(vendorContact vc);
	
	public List<vendorContact> getContactUsList();
	
	public void removeContactUs(int id);
	
	public void addSubscribe(String name, String email);
	
	public int getSubscriber(String email);
	
	public int getListById(String name);
	
	public int checkEmail(String email);
	
}
