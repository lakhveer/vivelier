package com.lakhi.realestate.vender.service;

import java.util.List;

import com.lakhi.realestate.admin.model.hotels;
import com.lakhi.realestate.vender.model.vendor;

public interface VendorService {

	public void addVendor(vendor v);
	
	public void forgotPassword(vendor v);
	
	public int checkEmail(String email);

	public vendor updateVendor(vendor v);
	
	public vendor updateVendorProfilePic(vendor v);

	public void deleteVendor(int id);

	public List<vendor> allVendors();

	public List<vendor> getVendorByEmail(String email);
	
	public String isLogin(String email, String password);
	
	public void createPassword(vendor v);

	public List<hotels> getAllHotels();
	
	public List<hotels> getHotelBySearch(String data);
	
	public void updateVendorPlan(String plan, String email);
	
	public List<hotels> advanceSearch(String hotel, String city, String country, String state, String rating, String zip);
	
	public List<hotels> countSearchHotels(String hotel, String city, String country);
	
	public void addLoginStatus(int loginId,String email, String sessionId);
	
	public Integer checkLoginStatusByEmail(String email);
	
	public void updateLoginStatus(String id);
	
	public String checkLoginStatus(String email);
	
}
