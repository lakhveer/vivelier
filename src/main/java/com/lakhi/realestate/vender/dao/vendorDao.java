package com.lakhi.realestate.vender.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.lakhi.realestate.admin.model.hotels;
import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.vender.model.vendor;
import com.lakhi.realestate.vender.service.VendorService;

public class vendorDao implements VendorService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs;

	@Override
	public void addVendor(vendor v) {

		String plan = "";
		if (v.getAmount().equals("5")) {
			plan = "Basic";
		} else if (v.getAmount().equals("10")) {
			plan = "Pro";
		} else {
			plan = "Preimum";
		}

		try {
			st = con.createStatement();
			String query = "insert into vendors values (null,'" + v.getFirst_name() + "','" + v.getLast_name() + "'"
					+ ",'" + v.getEmail() + "',MD5('" + v.getFirst_name() + "'),'" + v.getPhone() + "','" + plan
					+ "','img.png', '', '1',now() ,DATE_ADD(now(), INTERVAL 31 DAY), '1') ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public vendor updateVendor(vendor v) {

		try {

			st = con.createStatement();
			String query = "update vendors set vendor_password = MD5('" + v.getPassword() + "'),"
					+ "vendor_first_name = '" + v.getFirst_name() + "'," + "vendor_last_name = '" + v.getLast_name()
					+ "'," + "vendor_phone = '" + v.getPhone() + "'," + "vendor_plan = '" + v.getAmount()
					+ "', vendor_address = '" + v.getAddress() + "'  where vendor_id = " + v.getId();
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return v;
	}

	@Override
	public void deleteVendor(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<vendor> allVendors() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<vendor> getVendorByEmail(String email) {
		List<vendor> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select * from vendors where vendor_email = '" + email + "' ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String first_name = rs.getString(2);
				String last_name = rs.getString(3);
				String phone = rs.getString(6);
				String plan = rs.getString(7);
				String img = rs.getString(8);
				String address = rs.getString(9);
				int status = rs.getInt(10);
				String date = rs.getString(11);
				String expDate = rs.getString(12);

				String z[] = date.split("-");
				String d = z[2] + "-" + z[1] + "-" + z[0];

				String z1[] = expDate.split("-");
				String d1 = z1[2] + "-" + z1[1] + "-" + z1[0];

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				Date expdate = new Date();
				String edate = dateFormat.format(expdate);

				int a = Integer.parseInt(z[2] + z[1] + z[0]);
				int b = Integer.parseInt(z1[0] + z1[1] + z1[2]);

				DateFormat dd = new SimpleDateFormat("ddMMyyyy");
				int curDate = Integer.parseInt(dd.format(expdate));

				DateFormat dFormat = new SimpleDateFormat("yyyyMMdd");
				Date currentdate = new Date();
				// System.out.println(dFormat.format(currentdate)+"\n"+b);
				int curdate = Integer.parseInt(dFormat.format(currentdate));

				String vendorStatus = "";
				if (curdate < b) {
					vendorStatus = "Active";
					// System.out.println("A " + vendorStatus);
				} else {
					vendorStatus = "Expire";
					// System.out.println("B " + vendorStatus);
				}

				vendor v = new vendor();
				v.setId(id);
				v.setFirst_name(first_name);
				v.setLast_name(last_name);
				v.setEmail(email);
				v.setPhone(phone);
				v.setAmount(plan);
				v.setImage(img);
				v.setAddress(address);
				v.setStatus(status);
				v.setDate(d);
				v.setExpireDate(d1);
				v.setExpDate(edate);
				v.setVendorPaymentStatus(vendorStatus);

				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public String isLogin(String email, String password) {
		String aa = "";
		try {
			st = con.createStatement();
			String query = "select * from vendors where vendor_email = '" + email + "' and vendor_password = MD5('"
					+ password + "') ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				aa = rs.getString("vendor_status");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aa;
	}

	@Override
	public vendor updateVendorProfilePic(vendor v) {
		try {
			st = con.createStatement();
			String query = "update vendors set vendor_image = '" + v.getImage() + "'  where vendor_email = '"
					+ v.getEmail() + "' ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return v;
	}

	@Override
	public void createPassword(vendor v) {
		try {
			st = con.createStatement();
			String query = "update vendors set vendor_password = MD5('" + v.getPassword()
					+ "'),vendor_status = '1', payment='1' where vendor_email = '" + v.getEmail() + "' ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<hotels> getAllHotels() {
		List<hotels> l = new ArrayList<hotels>();

		try {

			st = con.createStatement();
			String query = " select * from hotelData order by id desc limit 50  ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);

				// System.out.println(h.getAddress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<hotels> getHotelBySearch(String data) {

		List<hotels> l = new ArrayList<hotels>();
		try {

			st = con.createStatement();
			// String query = " select * from hotelData where (country like '%"
			// + data + "%') or (city like'%" + data
			// + "%') or (hotelName like '%" + data + "%') limit 50 ";
			String query = " select * from hotelData where id in (" + data.replace("on,", "") + ") limit 50 ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);
				String zip = rs.getString(11);
				String state = rs.getString(9);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);
				h.setRemarks(remarks);
				h.setZip(zip);
				h.setState(state);

				l.add(h);

				// System.out.println(query);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void updateVendorPlan(String plan, String email) {

		try {

			st = con.createStatement();
			String query = " update vendors set vendor_plan = '" + plan
					+ "',date = now(), expDate = DATE_ADD(now(), INTERVAL 31 DAY), payment='1' where vendor_email = '"
					+ email + "' ";
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public List<hotels> advanceSearch(String hotel, String city, String country, String state, String rting,
			String zip) {
		List<hotels> l = new ArrayList<hotels>();

		String aa = "";
		if (!hotel.equals("") && !hotel.equals(null)) {
			aa = "HOTELNAMES like '%" + hotel + "%'";
		}
		if (!city.equals("")) {
			aa = "city like'%" + city + "%'";
		}
		if (!country.equals("")) {
			aa = "country like '%" + country + "%'";
		}

		try {
			st = con.createStatement();
			String query = " select * from hotelData where (country like '%" + country + "%') and (city like'%" + city
					+ "%') and (hotelName like '%" + hotel + "%') and (state like '%" + state
					+ "%') and (rating like '%" + rting + "%') and (zip like '%" + zip + "%') ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String hotelName = rs.getString(2);
				String city1 = rs.getString(8);
				String website = rs.getString(4);
				String email1 = rs.getString(5);
				String email2 = rs.getString(6);
				String address = rs.getString(7); // street address
				String telephone1 = rs.getString(12);
				String telephone2 = rs.getString(13);
				String country1 = rs.getString(10);
				String remarks = rs.getString(14);
				String rating = rs.getString(3);

				hotels h = new hotels();
				h.setId(id);
				h.setHotelName(hotelName);
				h.setRating(rating);
				h.setCity(city1);
				h.setWebsite(website);
				h.setEmail1(email1);
				h.setEmail2(email2);
				h.setAddress(address);
				h.setTelephone1(telephone1);
				h.setTelephone2(telephone2);
				h.setCountry(country1);
				h.setDescription(remarks);

				l.add(h);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public List<hotels> countSearchHotels(String hotel, String city, String country) {
		List<hotels> l = new ArrayList<hotels>();
		String aa = "";
		if (!hotel.equals("")) {
			aa = "HOTELNAMES like '%" + hotel + "%'";
		}
		if (!city.equals("")) {
			aa = "city like'%" + city + "%'";
		}
		if (!country.equals("")) {
			aa = "country like '%" + country + "%'";
		}
		try {
			st = con.createStatement();
			String query = "select COUNT(id) from hotelData where " + aa + " ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);

				hotels h = new hotels();
				h.setId(id);

				l.add(h);

			}
			// System.out.println(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public int checkEmail(String email) {
		int i = 0;

		try {

			st = con.createStatement();
			String query = "select vendor_email from vendors where  vendor_email = '" + email + "'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String mail = rs.getString("vendor_email");
				i = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	@Override
	public void forgotPassword(vendor v) {
		try {
			st = con.createStatement();
			String query = "update vendors set vendor_password = MD5('" + v.getPassword() + "') where vendor_email = '"
					+ v.getEmail() + "' ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void addLoginStatus(int loginId, String email, String sessionId) {
		try {
			st = con.createStatement();
			String query = "insert into loginStatus values (null," + loginId + ",'" + email + "','" + sessionId
					+ "','1')";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String checkLoginStatus(String email) {
		String z = "0";
		try {
			st = con.createStatement();
			String query = "select * from loginStatus where email = '" + email + "'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				z = rs.getString("status");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return z;
	}

	@Override
	public void updateLoginStatus(String id) {
		try {
			st = con.createStatement();
			String query = "delete from loginStatus where id = " + Integer.parseInt(id);
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Integer checkLoginStatusByEmail(String email) {

		int i = 0;

		try {
			st = con.createStatement();
			String query = "select * from loginStatus where email = '" + email + "'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				i = rs.getInt("id");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return i;
	}

}
