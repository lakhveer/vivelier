package com.lakhi.realestate.vender.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.vender.model.rating;
import com.lakhi.realestate.vender.model.vendorContact;
import com.lakhi.realestate.vender.service.contactService;

public class vendorContactDao implements contactService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs;

	@Override
	public void contactVendor(vendorContact vc) {
		try {
			st = con.createStatement();
			String query = "insert into contactVendor values (null, " + vc.getListId() + ",'" + vc.getName() + "','"
					+ vc.getEmail() + "','" + vc.getTitle() + "','" + vc.getDescription() + "',now()) ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void addRating(rating r) {
		try {
			st = con.createStatement();
			String query = "insert into rating values (null," + r.getListId() + ",'" + r.getRating() + "','"
					+ r.getVmail() + "','" + r.getTitle() + "','" + r.getDescription().replaceAll("'", "") + "', now() ) ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<rating> getRatingByList(String name) {
		List<rating> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select r.*,cl.title from rating r, createList cl where (r.listId = cl.id) and cl.title = '"
					+ name + "' order by id desc";
			rs = st.executeQuery(query);
			while (rs.next()) {
				 int id = rs.getInt("id");
				int listId = rs.getInt("listId");
				String rating = rs.getString("rating");
				String vmail = rs.getString("vmail");
				String ratingTitle = rs.getString("title");
				String description = rs.getString("description");
				String date = rs.getString("date");
				String listTitle = rs.getString(8);

				rating r = new rating();
				r.setId(id);
				r.setListId(listId);
				r.setRating(rating);
				r.setVmail(vmail);
				r.setTitle(ratingTitle);
				r.setDescription(description);
				r.setDate(date);
				r.setListTitle(listTitle);

				l.add(r);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public void contactUs(vendorContact vc) {
		try {
			st = con.createStatement();
			String query = "insert into contactus values (null, '"+vc.getFirstName()+"', '"+vc.getLastName()+"', '"+vc.getEmail()+"', '"+vc.getMessage().replaceAll("'", "%5")+"')";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<vendorContact> getContactUsList() {
		List<vendorContact> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select * from contactus order by id desc";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String firstName = rs.getString("firstName");
				String lastName = rs.getString("lastName");
				String email = rs.getString("email");
				String message = rs.getString("message");
				
				vendorContact v = new vendorContact();
				v.setId(id);
				v.setFirstName(firstName);
				v.setLastName(lastName);
				v.setEmail(email);
				v.setMessage(message.replaceAll("%5", "'"));
				
				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public void addSubscribe(String name, String email) {
		try {
			st = con.createStatement();
			String query = "insert into subscribe values (null, '"+name+"', '"+email+"')";
			st.execute(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public int getSubscriber(String email) {
		int i = 0;
		try {
			st = con.createStatement();
			String query = "select * from subscribe where email = '"+email+"'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				i = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return i;
	}

	@Override
	public int getListById(String name) {
		int i = 0;
		try {
			st = con.createStatement();
			String query = "select * from createList where title = '"+name.replaceAll("-", " ")+"'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				i = rs.getInt("id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return i;
	}

	@Override
	public void removeContactUs(int id) {
		try {
			st = con.createStatement();
			String query = "delete from contactus where id = " + id;
			st.execute(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	@Override
	public int checkEmail(String email) {
		int i = 0;
		try {
			st = con.createStatement();
			String query = "select vendor_email from vendors where vendor_email = '"+email+"' ";
			rs = st.executeQuery(query);
			while(rs.next()){
				i = 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return i;
	}

}
