package com.lakhi.realestate.vender.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.vender.model.listCategory;
import com.lakhi.realestate.vender.model.vendor;
import com.lakhi.realestate.vender.model.vendorListing;
import com.lakhi.realestate.vender.service.ListingService;

public class listingDao implements ListingService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs; 

	@Override
	public void addListingInVendor(vendorListing v) {
		int yr = 0;

		if (v.getSignupPlan().equals("Standard 1 Yr")) {
			yr = 365 * 1;
		} else if (v.getSignupPlan().equals("Standard 3 Yr")) {
			yr = 365 * 3;
		} else if (v.getSignupPlan().equals("Featured 1 Yr")) {
			yr = 365 * 1;
		} else if (v.getSignupPlan().equals("Free List")) {
			yr = 365 * 1;
		}

		try {
			st = con.createStatement();
			String query = "insert into vendors values (null,'" + v.getContactPerson() + "',''" + ",'" + v.getEmail()
					+ "',MD5('p@$$word'),'" + v.getPhone() + "','" + v.getSignupPlan() + "'," + "'img.png', '',"
							+ " '1',now() ,DATE_ADD(now(), INTERVAL " + yr + " DAY), '1') ";
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void createListing(vendorListing l) {
		
		int id = 0;
		String expDate = null;
		try {
			st = con.createStatement();
			String q = "select * from vendors ";
			rs = st.executeQuery(q);
			while (rs.next()) {
				id = rs.getInt("vendor_id");
				expDate = rs.getString("expDate");
			}
			//System.out.println("expDate = " + expDate);

			String query = "insert into createList values (null," + id + "," + l.getMainCategory() + ","
					+ l.getSubCategory() + ",'" + l.getMainImage() + "','" + l.getTitle().replaceAll("'", "") + "','" + l.getCompanyName()
					+ "','" + l.getFax() + "','" + l.getWebsite() + "','" + l.getDescription().replaceAll("'", "") + "','1','"+expDate+"','"+l.getAddress()+"','"+l.getCity()+"','"+l.getState()+"','"+l.getCountry()+"' ) ";
			
			//System.out.println(query);
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<listCategory> getMainCategories() {
		List<listCategory> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select * from listMainCategory";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String name = rs.getString(2);

				listCategory lc = new listCategory();
				lc.setMainCategoryId(id);
				lc.setCategoryName(name);

				l.add(lc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<listCategory> getSubCategories(String name1) {
		List<listCategory> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select sc.*,mc.* from listSubCategory sc,listMainCategory mc where sc.mainCategory = mc.id and mc.categoryName = '"+name1+"' " ;
			rs = st.executeQuery(query);
			while (rs.next()) {
				
				int subCategoryId = rs.getInt(1);
				String name = rs.getString(3);

				listCategory lc = new listCategory();
//				lc.setMainCategoryId(id);
				lc.setSubCategoryId(subCategoryId);
				lc.setSubCategoryName(name);

				l.add(lc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<String> getAllCountries() {

		List<String> l = new ArrayList<String>();
		String[] locales = Locale.getISOCountries();
		for (String countryCode : locales) {
			Locale obj1 = new Locale("countries", countryCode);
			l.add(obj1.getDisplayCountry());
		}

		return l;
	}

	@Override
	public List<vendorListing> getAllListing(String email) {
		List<vendorListing> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email from createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and v.vendor_email = '"
					+ email + "' ";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String mCategory = rs.getString("categoryName");
				String sCategory = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setCategoryName(mCategory);
				vl.setSubCategoryName(sCategory);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> getListById(int id) {
		List<vendorListing> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email from createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and cl.id = "
					+ id;
			rs = st.executeQuery(query);
			while (rs.next()) {
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String mCategory = rs.getString("categoryName");
				String sCategory = rs.getString("subCategory");
				String address = rs.getString("address");
				String city = rs.getString("city");
				String state = rs.getString("state");
				String country = rs.getString("country");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setCategoryName(mCategory);
				vl.setSubCategoryName(sCategory);
				vl.setAddress(address);
				vl.setCity(city);
				vl.setState(state);
				vl.setCountry(country);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void ListUpdate(vendorListing v) {

		try {
			st = con.createStatement();
			String query = "update createList set mCategory = " + v.getMainCategory() + ",sCategory="
					+ v.getSubCategory() + ", title='" + v.getTitle() + "'," + "website='" + v.getWebsite()
					+ "', description = '" + v.getDescription() + "', address = '"+v.getAddress()+"', city='"+v.getCity()+"', state='"+v.getState()+"', country='"+v.getCountry()+"'  where id =  " + v.getId();
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void updateCoverImg(int id, String file) {
		try {
			st = con.createStatement();
			String query = "update createList set image = '" + file + "' where id =  " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<vendorListing> getAllList() {
		List<vendorListing> l = new ArrayList<>();

		try {
			st = con.createStatement();
			//String query = "select cl.*,v.vendor_plan, mc.categoryName from createList cl, vendors v, listMainCategory mc where cl.vendorId = v.vendor_id  and cl.mCategory = mc.id and v.vendor_status = '1' order by id desc";
			String query = "select cl.*,v.vendor_plan, mc.categoryName from createList cl, vendors v, listMainCategory mc where cl.vendorId = v.vendor_id  and cl.mCategory = mc.id and v.vendor_status = '1' and (v.expDate > now()) and cl.list_stsatus='1' and v.vendor_plan != 'Free List' order by id desc";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String mCategory = rs.getString("categoryName");
				String vPlan = rs.getString("vendor_plan");
				String status = rs.getString("list_stsatus");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setCategoryName(mCategory);
				vl.setSignupPlan(vPlan);
				vl.setStatus(status);

				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<listCategory> getCategories() {
		List<listCategory> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select sc.*,mc.categoryName from listSubCategory sc, listMainCategory mc where sc.mainCategory = mc.id  order by mc.categoryName asc";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				int mainCategoryId = rs.getInt(2); 
				String name = rs.getString("categoryName");
				String subCategory = rs.getString("subCategory");

				listCategory lc = new listCategory();
				lc.setSubCategoryId(id);
				lc.setMainCategoryId(mainCategoryId);
				lc.setCategoryName(name);
				lc.setSubCategoryName(subCategory);

				l.add(lc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	@Override
	public List<vendorListing> getSingleList(String name) {
		List<vendorListing> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and cl.title = '"+name.replaceAll("-", " ")+"'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("address");
				String city = rs.getString("city");
				String state = rs.getString("state");
				String country = rs.getString("country");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setCity(city);
				vl.setState(state);
				vl.setCountry(country);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> ListingData(String mid, String sid) {
		 String[] z =   mid.split("-");
		 String[] z1 =  sid.split("-");
		 
		 String zz = "";
		 
		 if(z1[0].equals("adder") && z[0].equals("cate")){
			 zz += "(cl.city = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("cate") && z1[0].equals("state") ){
			 zz += "(cl.state = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("cate") && z1[0].equals("country") ){
			 zz += "(cl.country = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("adder") && z1[0].equals("state") ){
			 zz += "(cl.state = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.city = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("adder") && z1[0].equals("country") ){
			 zz += "(cl.country = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.city = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("state") && z1[0].equals("country") ){
			 zz += "(cl.country = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.state = '"+z[1].replaceAll("\\+", " ")+"')";
		 } else if(z[0].equals("cate") && z1[0].equals("sub") ){
			 zz += "(mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (sc.subCategory like '"+z1[1].replaceAll("\\+", " ")+"%')";
		 }
		 
		List<vendorListing> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from  createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and ("+zz+")";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id  = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("vendor_address");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");
				String subCategoryName = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);
				vl.setSubCategoryName(subCategoryName);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> mainListingData(String mid) {
		 String[] z =   mid.split(" ");
		 String data = null;
		 if(z[0].equals("adder")){
			 data = "(cl.city like '%"+z[1].replaceAll("-", ",")+"%')";
		 }
		 if(z[0].equals("cate")){
			 data = "(mc.categoryName like '%"+z[1].replaceAll("-", " ")+"%')";
		 }
		 if(z[0].equals("sub")){
			 data = "(sc.subCategory like '%"+z[1].replaceAll("-", " ")+"%')";
		 }
		 if(z[0].equals("state")){
			 data = "(cl.state like '%"+z[1].replaceAll("-", " ")+"%')";
		 }
		 if(z[0].equals("country")){
			 data = "(cl.country like '%"+z[1].replaceAll("-", " ")+"%')";
		 }
		 if(z[0].equals("title")){
			 data = "(cl.title like '"+z[1].replaceAll("-", " ")+"%')";
		 }
		List<vendorListing> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and "+data+"";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id  = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("vendor_address");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");
				String subCategoryName = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title.replaceAll(" ", "-"));
				vl.setTitle1(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);
				vl.setSubCategoryName(subCategoryName);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void deleteListing(int id) {
		try {
			st = con.createStatement();
			String query = "delete from createList where id = " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void createNewList(vendorListing l) {
		int id = 0;
		String expDate = null;
		try {
			st = con.createStatement();
			String q = "select * from vendors where vendor_email = '"+l.getEmail()+"' ";
			rs = st.executeQuery(q);
			while (rs.next()) {
				id = rs.getInt("vendor_id");
				expDate = rs.getString("expDate");
			}
			
			String query = "insert into createList values (null," + id + "," + l.getMainCategory() + ","
					+ l.getSubCategory() + ",'" + l.getMainImage() + "','" + l.getTitle().replaceAll("'", "") + "','" + l.getCompanyName()
					+ "','" + l.getFax() + "','" + l.getWebsite() + "','" + l.getDescription().replaceAll("'", "") + "','1','"+expDate+"','"+l.getAddress()+"','"+l.getCity()+"','"+l.getState()+"','"+l.getCountry()+"' ) ";
			
			//System.out.println(query);
			st.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public int getMainCategorieByName(String name) {
		int i = 0;
		try {
			st = con.createStatement();
			String query = "select * from listMainCategory where categoryName = '"+name+"'";
			rs = st.executeQuery(query);
			while (rs.next()) {
				i = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	@Override
	public List<vendorListing> getListingDataByAddress(String data) {
		List<vendorListing> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and (cl.address like '%"+data+"%')";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id  = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("vendor_address");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");
				String subCategoryName = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);
				vl.setSubCategoryName(subCategoryName);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> getListAddress() {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT address from createList";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String address = rs.getString("address");
				
				vendorListing vl = new vendorListing();
				vl.setAddress(address);
				
				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<vendorListing> getListCity() {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT city from createList";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String address = rs.getString("city");
				
				vendorListing vl = new vendorListing();
				vl.setCity(address);
				
				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<vendorListing> getListState() {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT state from createList";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String address = rs.getString("state");
				
				vendorListing vl = new vendorListing();
				vl.setState(address);
				
				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<vendorListing> getListCountry() {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT country from createList";
			rs = st.executeQuery(query);
			while (rs.next()) {
				String address = rs.getString("country");
				
				vendorListing vl = new vendorListing();
				vl.setCountry(address);
				
				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<vendorListing> ListingByState(String mid, String sid, String sta) {
		
		//System.out.println("main = " + mid + "\n Sub = " + sid + "\n other = " + sta);
		
		String[] z =   mid.split("-");
		String[] z1 =  sid.split("-");
		String[] z2 =  sta.split("-");
		
		 String zz = "";
		 
		 if(z1[0].equals("adder") && z[0].equals("cate") && z2[0].equals("state")){
			 zz += "(cl.city = '"+z1[1].replaceAll("-", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("-", " ")+"') ";
			 zz += " and (cl.state = '"+z2[1].replaceAll("\\+", " ")+"') ";
		 } else if(z[0].equals("cate") && z1[0].equals("state") && z2[0].equals("country")){
			 zz += "(cl.country = '"+z2[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.state = '"+z1[1].replaceAll("\\+", " ")+"') ";
		 } else if(z1[0].equals("state") && z2[0].equals("country") && z[0].equals("adder")){
			 zz += "(cl.country = '"+z2[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.city = '"+z[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.state = '"+z1[1].replaceAll("\\+", " ")+"') ";
		 } else if(z2[0].equals("country") && z1[0].equals("adder") && z[0].equals("cate")){
			 zz += "(cl.country = '"+z2[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (cl.city = '"+z1[1].replaceAll("\\+", " ")+"') ";
			 zz += " and (mc.categoryName = '"+z[1].replaceAll("\\+", " ")+"') ";
		 }
		 
		List<vendorListing> l = new ArrayList<>();

		try {

			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from  createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and ("+zz+")";
			//System.out.println(query);
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id  = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("vendor_address");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");
				String subCategoryName = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);
				vl.setSubCategoryName(subCategoryName);

				l.add(vl);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> ListingByCountry(String mid, String sid, String sta, String co) {
		String[] z =   mid.split("-");
		 String[] z1 =  sid.split("-");
		 String[] z2 =  sta.split("-");
		 String[] z3 =  co.split("-");
		 
		 String data = null;
		 String data1 = null;
		 String data2 = null;
		 String data3 = null;
		 if(z1[0].equals("adder")){
			 data = "(cl.city like '"+z1[1].replaceAll("\\+", ",")+"%')";
		 }
		 if(z[0].equals("cate")){
			 data1 = "(mc.categoryName like '%"+z[1].replaceAll("-", " ")+"%')";
		 }
		 if(z2[0].equals("state")){
			 System.out.println("STATE = " + z2[1]);
			 data2 = "(cl.state like '"+z2[1].replaceAll("-", " ")+"%')";
		 }
		 if(z3[0].equals("country")){
			 System.out.println("COUNTRY = " + z3[1]);
			 data3 = "(cl.country like '"+z3[1].replaceAll("-", " ")+"%')";
		 }
		List<vendorListing> l = new ArrayList<>();

		try {
			st = con.createStatement();
			String query = "select cl.*,mc.categoryName,sc.subCategory,v.vendor_email,v.vendor_phone,v.vendor_address,v.vendor_first_name,mc.categoryName from  createList cl,listMainCategory mc,listSubCategory sc,vendors v where (cl.mCategory = mc.id)and (cl.sCategory = sc.id) and (cl.vendorId = v.vendor_id) and ("+data1.replaceAll("\\+", " ")+" and "+data+" and "+data2+" and "+data3+")";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id  = rs.getInt("id");
				String image = rs.getString("image");
				String title = rs.getString("title");
				String website = rs.getString("website");
				String description = rs.getString("description");
				String email = rs.getString("vendor_email");
				String phone = rs.getString("vendor_phone");
				String address = rs.getString("vendor_address");
				String vendorName = rs.getString("vendor_first_name");
				String categoryName = rs.getString("categoryName");
				String subCategoryName = rs.getString("subCategory");

				vendorListing vl = new vendorListing();
				vl.setId(id);
				vl.setMainImage(image);
				vl.setTitle(title);
				vl.setWebsite(website);
				vl.setDescription(description);
				vl.setEmail(email);
				vl.setPhone(phone);
				vl.setAddress(address);
				vl.setContactPerson(vendorName);
				vl.setCategoryName(categoryName);
				vl.setSubCategoryName(subCategoryName);

				l.add(vl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<vendorListing> keywordSearch(String keywords) {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT c.title from createList c, listMainCategory m where (c.mCategory = m.id) and (c.title like '%"+keywords+"%')";
			rs = st.executeQuery(query);
			while(rs.next()){
				String title = rs.getString("title");
				vendorListing v = new vendorListing();
				v.setTitle(title);
				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

	@Override
	public List<vendorListing> vListkeywordSearch(String keywords, String email) {
		List<vendorListing> l = new ArrayList<>();
		try {
			st = con.createStatement();
			String query = "select DISTINCT c.title from createList c, listMainCategory m, vendors v where (c.mCategory = m.id) and (c.vendorId = v.vendor_id) and (c.title like '%"+keywords+"%') and v.vendor_email = '"+email+"'";
			rs = st.executeQuery(query);
			while(rs.next()){
				String title = rs.getString("title");
				vendorListing v = new vendorListing();
				v.setTitle(title);
				l.add(v);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l;
	}

}
