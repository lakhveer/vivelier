package com.lakhi.realestate.vender.model;

public class vendorContact {

	private int id;
	private int listId;
	private String name;
	private String email;
	private String title;
	private String description;
	private String date;
	private String vEmail;
	
	private String firstName;
	private String lastName;
	private String message;

	public vendorContact() {
	}

	public vendorContact(int id, int listId, String name, String email, String title, String description, String date,
			String vEmail,String firstName,String lastName,String message) {
		super();
		this.id = id;
		this.listId = listId;
		this.name = name;
		this.email = email;
		this.title = title;
		this.description = description;
		this.date = date;
		this.vEmail = vEmail;
		this.firstName = firstName;
		this.lastName = lastName;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getvEmail() {
		return vEmail;
	}

	public void setvEmail(String vEmail) {
		this.vEmail = vEmail;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	

}
