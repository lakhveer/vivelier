package com.lakhi.realestate.vender.model;

public class listCategory {

	public int mainCategoryId;
	public String categoryName;
	public int subCategoryId;
	public String subCategoryName;

	public listCategory() {

	}

	public listCategory(int mainCategoryId, String categoryName, int subCategoryId, String subCategoryName) {
		super();
		this.mainCategoryId = mainCategoryId;
		this.categoryName = categoryName;
		this.subCategoryId = subCategoryId;
		this.subCategoryName = subCategoryName;
	}

	public int getMainCategoryId() {
		return mainCategoryId;
	}

	public void setMainCategoryId(int mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

}
