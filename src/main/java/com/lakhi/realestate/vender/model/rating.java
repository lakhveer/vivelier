package com.lakhi.realestate.vender.model;

public class rating {
	private int id;
	private int listId;
	private String rating;
	private String vmail;
	private String title;
	private String description;
	private String date;
	private String listTitle;
	private String vName;
	private String listName;

	public rating() {

	}

	public rating(int id, int listId, String vmail, String title, String description, String date, String listTitle,
			String rating, String vName,String listName) {
		super();
		this.id = id;
		this.listId = listId;
		this.rating = rating;
		this.vmail = vmail;
		this.title = title;
		this.description = description;
		this.date = date;
		this.listTitle = listTitle;
		this.vName = vName;
		this.listName = listName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getListId() {
		return listId;
	}

	public void setListId(int listId) {
		this.listId = listId;
	}

	public String getVmail() {
		return vmail;
	}

	public void setVmail(String vmail) {
		this.vmail = vmail;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getListTitle() {
		return listTitle;
	}

	public void setListTitle(String listTitle) {
		this.listTitle = listTitle;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getvName() {
		return vName;
	}

	public void setvName(String vName) {
		this.vName = vName;
	}
	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}
}
