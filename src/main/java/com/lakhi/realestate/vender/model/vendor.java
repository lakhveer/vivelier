package com.lakhi.realestate.vender.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class vendor {

	private int id;
	private String first_name;
	private String last_name;
	private String email;
	private String password;
	private String phone;
	private String date;
	private String amount;
	private String image;
	private String surl;
	private String furl;
	private String txnid;
	private String hash;
	private String key;
	private int status;
	private String address;
	private String expireDate;
	private String expDate;
	private String payment;
	private String vendorPaymentStatus;

	public vendor(int id, String first_name, String last_name, String email, String password, String phone, String date,
			String amount, String image, int status, String address, String expireDate, String expDate,
			String payment, String vendorPaymentStatus) {
		super();
		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.password = password;
		this.phone = phone;
		this.date = date;
		this.amount = amount;
		this.image = image;
		this.status = status;
		this.address = address;
		this.expireDate = expireDate;
		this.expDate = expDate;
		this.payment = payment;
		this.vendorPaymentStatus = vendorPaymentStatus;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public String getSurl() {
		return surl;
	}

	public String getFurl() {
		return furl;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public vendor() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getVendorPaymentStatus() {
		return vendorPaymentStatus;
	}

	public void setVendorPaymentStatus(String vendorPaymentStatus) {
		this.vendorPaymentStatus = vendorPaymentStatus;
	}
	
}
