package com.lakhi.realestate.vender.model;

import java.io.Serializable;

public class vendorListing implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private int mainCategory;
	private int subCategory;
	private String categoryName;
	private String subCategoryName;
	private String mainImage;
	private String otherImages;
	private String signupPlan;
	private String title;
	private String title1;
	private String companyName;
	private String contactPerson;
	private String Address;
	private String city;
	private String state;
	private String country;
	private String phone;
	private String fax;
	private String email;
	private String website;
	private String description;
	private String status;

	public vendorListing() {

	}

	public vendorListing(int id, int mainCategory, int subCategory, String mainImage, String otherImages,
			String signupPlan, String title, String title1, String companyName, String contactPerson, String address, String city,
			String state, String country, String phone, String fax, String email, String website, String description,String categoryName, String subCategoryName,String status) {
		super();
		this.id = id;
		this.mainCategory = mainCategory;
		this.subCategory = subCategory;
		this.mainImage = mainImage;
		this.otherImages = otherImages;
		this.signupPlan = signupPlan;
		this.title = title;
		this.title1 = title1;
		this.companyName = companyName;
		this.contactPerson = contactPerson;
		this.Address = address;
		this.city = city;
		this.state = state;
		this.country = country;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
		this.website = website;
		this.description = description;
		this.categoryName = categoryName;
		this.subCategoryName = subCategoryName;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMainCategory() {
		return mainCategory;
	}

	public void setMainCategory(int mainCategory) {
		this.mainCategory = mainCategory;
	}

	public int getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(int subCategory) {
		this.subCategory = subCategory;
	}

	public String getMainImage() {
		return mainImage;
	}

	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}

	public String getOtherImages() {
		return otherImages;
	}

	public void setOtherImages(String otherImages) {
		this.otherImages = otherImages;
	}

	public String getSignupPlan() {
		return signupPlan;
	}

	public void setSignupPlan(String signupPlan) {
		this.signupPlan = signupPlan;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}
}
