package com.lakhi.realestate.freeVendors.models;

import java.io.Serializable;
import java.util.List;

public class Ads implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private int vendorId;
	private String title;
	private String description;
	private List features;
	private String list;
	private String img;
	private String date;

	public Ads() {

	}

	public Ads(int id, String title, String description, List features, String img, String date, String list, int vendorId) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.features = features;
		this.img = img;
		this.date = date;
		this.list = list;
		this.vendorId = vendorId;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List getFeatures() {
		return features;
	}

	public void setFeatures(List features) {
		this.features = features;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
