package com.lakhi.realestate.freeVendors.services;

import java.util.List;

import com.lakhi.realestate.freeVendors.models.Ads;

public interface AdService {

	public void createAds(Ads a);
	
	public List<Ads> getAllAds(String email);
	
	public List<Ads> getAdById(int id);
	
	public void deleteAd(int id);
	
	public void updateAd(Ads a);
}
