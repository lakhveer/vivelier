package com.lakhi.realestate.freeVendors.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.lakhi.realestate.dbconfig.dbconfig;
import com.lakhi.realestate.freeVendors.models.Ads;
import com.lakhi.realestate.freeVendors.services.AdService;

public class AdsDao implements AdService {

	Connection con = dbconfig.getConnection();
	Statement st;
	ResultSet rs;

	@Override
	public void createAds(Ads a) {
		try {
			st = con.createStatement();
			String query = "insert into ads values (null,"+a.getVendorId()+",'" + a.getTitle() + "','" + a.getDescription().replaceAll("'", "") + "'" + ",'"
					+ a.getFeatures() + "','" + a.getImg() + "',now()) ";
			System.out.println(query);
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Ads> getAllAds(String email) {
		List<Ads> l = new ArrayList<Ads>();

		try {
			st = con.createStatement();
			String query = "select a.* from ads a, vendors v where a.vendorId = v.vendor_id and v.vendor_email = '"+email+"' order by id desc";
			rs = st.executeQuery(query);
			while (rs.next()) {
				int id = rs.getInt(1);
				String title = rs.getString(3);
				String description = rs.getString(4);
				String features = rs.getString(5);
				String img = rs.getString(6);
				String date = rs.getString(7);

				List aa = new ArrayList();
				String[] z = features.split("==");
				for (String a : z) {
					aa.add(a);
				}

				Ads a = new Ads();
				a.setId(id);
				a.setTitle(title);
				a.setDescription(description);
				a.setFeatures(aa);
				a.setImg(img);
				a.setDate(date);

				l.add(a);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public List<Ads> getAdById(int id) {
		List<Ads> l = new ArrayList<Ads>();

		try {
			st = con.createStatement();
			String query = "select * from ads where id = " + id;
			rs = st.executeQuery(query);
			while (rs.next()) {
				String title = rs.getString(3);
				String description = rs.getString(4);
				String features = rs.getString(5);
				String img = rs.getString(6);
				String date = rs.getString(7);

				String data = features.replace("]", "");
				List aa = new ArrayList();
				String[] z = data.split("==");
				for (String a : z) {
					aa.add(a);
				}

				Ads a = new Ads();
				a.setId(id);
				a.setTitle(title);
				a.setDescription(description);
				a.setFeatures(aa);
				a.setImg(img);
				a.setDate(date);

				l.add(a);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}

		return l;
	}

	@Override
	public void deleteAd(int id) {
		try {
			st = con.createStatement();
			String query = "delete from ads where id = " + id;
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateAd(Ads a) {
		try {
			st = con.createStatement();
			String query = "update ads set title = '" + a.getTitle() + "',description = '" + a.getDescription()
					+ "',features = '" + a.getFeatures() + "'" + "where id = " + a.getId();
			st.execute(query);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(con != null) {
					con.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
